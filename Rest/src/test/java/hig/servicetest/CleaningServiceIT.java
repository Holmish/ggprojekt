/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.servicetest;

import hig.BasicApplication;
import hig.domain.Cleaning;
import hig.domain.Name;
import hig.domain.Person;
import hig.domain.Room;
import hig.repository.CleaningRepository;
import hig.repository.PersonRepository;
import hig.repository.RoomRepository;
import hig.service.cleaning.CleaningDTO;
import hig.service.cleaning.CleaningService;
import hig.service.person.PersonMapper;
import hig.service.room.RoomMapper;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author 21masu04
 */
@TestPropertySource(locations = "classpath:test.application.properties")
@SpringBootTest(classes = BasicApplication.class)
@ExtendWith(SpringExtension.class)
public class CleaningServiceIT {

    @Autowired
    private CleaningService cleaningService;

    @Autowired
    private CleaningRepository cleaningRepo;

    @Autowired
    private RoomRepository testRoomRepository;

    @Autowired
    private PersonRepository testPersonRepository;

    private Person testPerson;
    private Person testPerson2;
    private Room testRoom;
    private Room testRoom2;

    @Autowired
    private RoomMapper roomMapper;
    @Autowired
    private PersonMapper personMapper;

    private Cleaning cleaning;
    private Cleaning cleaningTwo;

    public CleaningServiceIT() {

    }

    @BeforeEach
    public void setUp() {

        testRoom = testRoomRepository.findById(1L).orElseThrow();
        testRoom2 = testRoomRepository.findById(12L).orElseThrow();
        testPerson = testPersonRepository.findByTag("7a3b09b9-e56c-49fb-a1d3-7fb9d61efabc");
        testPerson2 = testPersonRepository.findByTag("8a3b09b9-e56c-49fb-a1d3-7fb9d61efabc");
    }

    @AfterEach
    public void tearDown() {

    }

    /**
     * Test of getCleaningsByRoomIdAndDays method, of class CleaningService.
     */
    @Test
    public void testFindCleaningsByRoomIdAndDays() {
        System.out.println("FindCleaningsByRoomIdAndDays");

        List<CleaningDTO> expResult = List.of(new CleaningDTO(roomMapper.toRoomDto(testRoom2),
                personMapper.toSimplePersonDto(testPersonRepository.findByTag("88fc581a-a9db-4f1f-990d-d7c814bdf9c4")),
                LocalDateTime.of(2024, 5, 15, 11, 55, 56, 0)),
                new CleaningDTO(roomMapper.toRoomDto(testRoom2),
                        personMapper.toSimplePersonDto(testPersonRepository.findByTag("8a3b09b9-e56c-49fb-a1d3-7fb9d61efabc")),
                        LocalDateTime.of(2024, 5, 28, 11, 55, 56, 0)));

        List<CleaningDTO> result = cleaningService.findByRoomIdAndDays(12L, 1000L);

        assertEquals(expResult, result);

    }

    /**
     * Test of getCleaningsByRoomIdAndDaysWithNegativeDays method, of class
     * CleaningService.
     */
    @Test
    public void testFindCleaningsByRoomIdAndDaysWithNegativeDays() {
        System.out.println("FindCleaningsByRoomIdAndDaysWithNegativeDays");
        assertThrows(IllegalArgumentException.class, () -> cleaningService.findByRoomIdAndDays(1L, -20L));

    }

    /**
     * Test of findByPersonIdAndINtervalFromNullToNull method, of class
     * CleaningService.
     */
    @Test
    public void testfindByPersonIdAndIntervalFromNullToNull() {
        List<CleaningDTO> expResult = List.of(
                new CleaningDTO(roomMapper.toRoomDto(testRoom), personMapper.toSimplePersonDto(testPerson), LocalDateTime.of(2024, 04, 29, 10, 06, 00, 000000)),
                new CleaningDTO(roomMapper.toRoomDto(new Room(new Name("mAx Burgers"), "mAx, dåliga burgare och ej marknadspriser")), personMapper.toSimplePersonDto(testPerson), LocalDateTime.of(2024, 05, 2, 9, 50, 0, 000000)));
        List<CleaningDTO> result = cleaningService.findByPersonIdAndInterval("7a3b09b9-e56c-49fb-a1d3-7fb9d61efabc", null, null);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByPersonIdAndINtervalFromNullToGivenDate method, of class
     * CleaningService.
     */
    @Test
    public void testfindByPersonIdAndIntervalFromNullToGivenDate() {
        List<CleaningDTO> expResult = List.of(
                new CleaningDTO(roomMapper.toRoomDto(testRoom), personMapper.toSimplePersonDto(testPerson), LocalDateTime.of(2024, 04, 29, 10, 06, 00, 000000)),
                new CleaningDTO(roomMapper.toRoomDto(new Room(new Name("mAx Burgers"), "mAx, dåliga burgare och ej marknadspriser")), personMapper.toSimplePersonDto(testPerson), LocalDateTime.of(2024, 05, 2, 9, 50, 0, 000000)));
        List<CleaningDTO> result = cleaningService.findByPersonIdAndInterval("7a3b09b9-e56c-49fb-a1d3-7fb9d61efabc", null, LocalDateTime.of(2024, 05, 02, 10, 00, 00, 000000));
        assertEquals(expResult, result);
    }

    /**
     * Test of findByPersonIdAndINtervalFromGivenDayToNull method, of class
     * CleaningService.
     */
    @Test
    public void testfindByPersonIdAndIntervalFromGivenDateToNull() {
        List<CleaningDTO> expResult = List.of(
                new CleaningDTO(roomMapper.toRoomDto(new Room(new Name("mAx Burgers"), "mAx, dåliga burgare och ej marknadspriser")), personMapper.toSimplePersonDto(testPerson), LocalDateTime.of(2024, 05, 2, 9, 50, 0, 000000)));
        List<CleaningDTO> result = cleaningService.findByPersonIdAndInterval("7a3b09b9-e56c-49fb-a1d3-7fb9d61efabc", LocalDateTime.of(2024, 05, 01, 10, 00, 00, 000000), null);
        assertEquals(expResult, result);
    }

    /**
     * Test of findByPersonIdAndINtervalFromGivenDayeToGivenDate method, of
     * class CleaningService.
     */
    @Test
    public void testfindByPersonIdAndIntervalFromGivenDateToGivenDate() {
        List<CleaningDTO> expResult = List.of(
                new CleaningDTO(roomMapper.toRoomDto(testRoom), personMapper.toSimplePersonDto(testPerson), LocalDateTime.of(2024, 04, 29, 10, 06, 00, 000000)),
                new CleaningDTO(roomMapper.toRoomDto(new Room(new Name("mAx Burgers"), "mAx, dåliga burgare och ej marknadspriser")), personMapper.toSimplePersonDto(testPerson),
                        LocalDateTime.of(2024, 05, 2, 9, 50, 00, 000000)));
        List<CleaningDTO> result = cleaningService.findByPersonIdAndInterval("7a3b09b9-e56c-49fb-a1d3-7fb9d61efabc", LocalDateTime.of(2024, 04, 28, 10, 00, 00, 000000), LocalDateTime.of(2024, 05, 02, 11, 45, 00, 000000));
        assertEquals(expResult, result);
    }
}
