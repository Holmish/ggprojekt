/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.servicetest;

import hig.BasicApplication;
import hig.domain.Person;
import hig.domain.Salary;
import hig.repository.PersonRepository;
import hig.repository.SalaryRepository;
import hig.service.person.PersonMapper;
import hig.service.person.SimplePersonDTO;
import hig.service.salary.SalaryDTO;
import hig.service.salary.SalaryMapper;
import hig.service.salary.SalaryService;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author 21emho01
 */
@TestPropertySource(locations = "classpath:test.application.properties")
@SpringBootTest(classes = BasicApplication.class)
@ExtendWith(SpringExtension.class)
public class SalaryServiceIT {

    @Autowired
    private SalaryService salaryService;

    @Autowired
    private SalaryRepository salaryRepo;
    
    @Autowired 
    private SalaryMapper salaryMapper;

    @Autowired
    private PersonRepository personRepo;

    @Autowired
    private PersonMapper personMapper;
    
    private Person testPerson;
    private Person testPerson2;
    private Person testPerson3;

    private SimplePersonDTO claraFarnstrandDTO;
    private SimplePersonDTO emilHolmDTO;
    private SimplePersonDTO rudolfAnderssonDTO;

    public SalaryServiceIT() {
    }

    @BeforeEach
    public void setUp() {
        testPerson = personRepo.findByTag("8a3b09b9-e56c-49fb-a1d3-7fb9d61efabc");
        testPerson2 = personRepo.findByTag("7a3b09b9-e56c-49fb-a1d3-7fb9d61efabc");
        testPerson3 = personRepo.findByTag("88fc581a-a9db-4f1f-990d-d7c814bdf9c4");
        
        claraFarnstrandDTO = personMapper.toSimplePersonDto(testPerson);
        emilHolmDTO = personMapper.toSimplePersonDto(testPerson2);
        rudolfAnderssonDTO = personMapper.toSimplePersonDto(testPerson3);
    }

    @AfterEach
    public void tearDown() {
        salaryRepo.deleteAll();
    }

    /**
     * Test of find method, of class SalaryService.
     */
    @Test
    public void testFind() {
        System.out.println("testFind");
        Salary salary = salaryService.create(testPerson.getId());
        SalaryDTO expResult = new SalaryDTO(claraFarnstrandDTO, LocalDateTime.now().withSecond(0).withNano(0), 200);
        SalaryDTO result = salaryService.find(salary.getId());

        assertEquals(expResult, result);
    }

    /**
     * Test of findAll method, of class SalaryService.
     */
    @Test
    public void testFindAll() {
        System.out.println("testFindAll");
        salaryService.create(testPerson.getId());
        salaryService.create(testPerson2.getId());
        List<SalaryDTO> expResult = List.of(new SalaryDTO(claraFarnstrandDTO, LocalDateTime.now().withSecond(0).withNano(0), 200),
                new SalaryDTO(emilHolmDTO, LocalDateTime.now().withSecond(0).withNano(0), 250));
        List<SalaryDTO> result = salaryService.findAll();

        assertEquals(expResult, result);
    }

    /**
     * Test of CreateNoPreviousPayout method, of class SalaryService.
     */
    @Test
    public void testCreateNoPreviousPayout() {
        System.out.println("testCreateNoPreviousPayout");
        SalaryDTO expResult = new SalaryDTO(rudolfAnderssonDTO,
                LocalDateTime.now().withSecond(0).withNano(0), 100);
        SalaryDTO result = salaryMapper.toSalaryDto(salaryService.create(personRepo.findByTag("88fc581a-a9db-4f1f-990d-d7c814bdf9c4").getId()));

        assertEquals(expResult, result);
    }

    /**
     * Test of create method, of class SalaryService.
     */
    @Test
    public void testCreatePreviousPayoutNoNewSalary() {
        System.out.println("testCreatePreviousPayoutNoNewSalary");
        SalaryDTO expResult = new SalaryDTO(claraFarnstrandDTO,
                LocalDateTime.now().withSecond(0).withNano(0), 200);
        SalaryDTO result = salaryMapper.toSalaryDto(salaryService.create(226L));
        
        assertEquals(expResult, result);
    }

    /**
     * Test of CreatePreviousPayoutNewSalary method, of class SalaryService.
     */
    @Test
    public void testCreateWithChangingSalary() {
        System.out.println("testCreateWithChangingSalary");
        SalaryDTO expResult = new SalaryDTO(emilHolmDTO, LocalDateTime.now().withSecond(0).withNano(0), 250);
        SalaryDTO result = salaryMapper.toSalaryDto(salaryService.create(testPerson2.getId()));
        
        assertEquals(expResult, result);
    }
}
