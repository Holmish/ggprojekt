package hig.servicetest;

import hig.BasicApplication;
import hig.domain.Name;
import hig.domain.Person;
import hig.domain.Role;
import hig.domain.SalaryManager;
import hig.repository.PersonRepository;
import hig.service.person.AdvancedPersonDTO;
import hig.service.person.PersonDTO;
import hig.service.person.SimplePersonDTO;
import hig.service.person.PersonMapper;
import hig.service.person.PersonService;
import jakarta.persistence.EntityNotFoundException;
import java.util.List;
import java.time.LocalDateTime;
import static org.assertj.core.api.ClassBasedNavigableListAssert.assertThat;
import static org.hamcrest.CoreMatchers.hasItems;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * @author 21emho01
 */
@SpringBootTest(classes = BasicApplication.class)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:test.application.properties")
public class PersonServiceIT {

    private String callerTagUser = "8a3b09b9-e56c-49fb-a1d3-7fb9d61efabc";
    private String callerTagAdmin = "7a3b09b9-e56c-49fb-a1d3-7fb9d61efabc";
    String targetTag = callerTagAdmin;

    @Autowired
    private PersonService personService;

    private Person person;
    private Person personTwo;

    @Autowired
    private PersonMapper mapper;

    @Autowired
    private PersonRepository repository;

    private Person testPerson;
    private List<SimplePersonDTO> personList;

    public PersonServiceIT() {

    }

    @BeforeEach
    public void setUp() {

    }

    @AfterEach
    public void tearDown() {

    }

    /**
     * Test of findAdvancedPersonDTO method, of class PersonService.
     */
    @Test
    public void testFindAdvancedPersonDTO() {

        PersonDTO expResult = new AdvancedPersonDTO("Emil Holm", 1999, Role.ADMINISTRATOR, targetTag);
        PersonDTO result = personService.find(targetTag, callerTagAdmin);

        assertEquals(expResult, result);
        assertEquals(AdvancedPersonDTO.class, result.getClass());

        assertThrows(EntityNotFoundException.class, () -> personService.find("blabalbalbal", "hejehej"));

    }

    /**
     * Test of findSimplePersonDTO method, of class PersonService.
     */
    @Test
    public void testFindSimplePersonDTO() {

        PersonDTO expResult = new SimplePersonDTO("Emil Holm", 1999, Role.ADMINISTRATOR);
        PersonDTO result = personService.find(targetTag, callerTagUser);

        assertEquals(expResult, result);
        assertEquals(SimplePersonDTO.class, result.getClass());

    }

    /**
     * Test of findAllAdvancedPersonDTO method, of class PersonService.
     */
    @Test
    public void testFindAllAdvancedPersonDTO() {
        System.out.println("findAllAdvancedPersonDTO");

        List<PersonDTO> expResult = List.of(new AdvancedPersonDTO("Emil Holm", 1999, Role.ADMINISTRATOR, "7a3b09b9-e56c-49fb-a1d3-7fb9d61efabc"),
                new AdvancedPersonDTO("Clara Färnstrand", 1999, Role.USER, "8a3b09b9-e56c-49fb-a1d3-7fb9d61efabc"),
                new AdvancedPersonDTO("Rudolf Andersson", 1967, Role.USER, "88fc581a-a9db-4f1f-990d-d7c814bdf9c4"),
                new AdvancedPersonDTO("Test Persson", 1999, Role.ADMINISTRATOR, "testTag"));
        List<PersonDTO> result = personService.findAll(callerTagAdmin);

        boolean allAdvancedDTOs = result.stream()
                .allMatch(dto -> dto instanceof AdvancedPersonDTO);

        assertTrue(allAdvancedDTOs);

        assertEquals(expResult, result);
    }

    /**
     * Test of findAllSimplePersonDTO method, of class PersonService.
     */
    @Test
    public void testFindAllSimplePersonDTO() {
        System.out.println("findAllSimplePersonDTO");

        List<PersonDTO> expResult = List.of(new SimplePersonDTO("Emil Holm", 1999, Role.ADMINISTRATOR),
                new SimplePersonDTO("Clara Färnstrand", 1999, Role.USER),
                new SimplePersonDTO("Rudolf Andersson", 1967, Role.USER),
                new SimplePersonDTO(("Test Persson"), 1999, Role.ADMINISTRATOR));
        List<PersonDTO> result = personService.findAll(callerTagUser);

        boolean allSimpleDTOs = result.stream()
                .allMatch(dto -> dto instanceof SimplePersonDTO);

        assertTrue(allSimpleDTOs);

        assertEquals(expResult, result);
    }

    /**
     * Test of create method, of class PersonService.
     */
    @Test
    public void testCreate() {
        testPerson = new Person(new Name("Minch"), new Name("Yoda"), 1877, Role.USER, new SalaryManager(LocalDateTime.of(2025, 04, 14, 00, 00, 00, 000000), 400, 500));
        Person expResult = testPerson;
        Person result = personService.create(testPerson);
        assertEquals(expResult, result);

        Person deletePerson = repository.findByTag(testPerson.getTag());

        personService.delete(deletePerson.getTag());

    }

    /**
     * Test of create with firstName null method, of class PersonService.
     */
    @Test
    public void testCreateWithFirstNameNull() {
        testPerson = new Person(null, new Name("Yoda"), 1877, Role.USER, new SalaryManager(LocalDateTime.of(2025, 04, 14, 00, 00, 00, 000000), 400, 500));
        assertThrows(IllegalArgumentException.class, () -> personService.create(testPerson));
    }

    /**
     * Test of create with lastName null method, of class PersonService.
     */
    @Test
    public void testCreateWithLastNameNull() {
        testPerson = new Person(new Name("Minch"), null, 1877, Role.USER, new SalaryManager(LocalDateTime.of(2025, 04, 14, 00, 00, 00, 000000), 400, 500));
        assertThrows(IllegalArgumentException.class, () -> personService.create(testPerson));
    }

    /**
     * Test of create with lastName null method, of class PersonService.
     */
    @Test
    public void testCreateWithFirstNameAndLastNameNull() {
        testPerson = new Person(null, null, 1877, Role.USER, new SalaryManager(LocalDateTime.of(2025, 04, 14, 00, 00, 00, 000000), 400, 500));
        assertThrows(IllegalArgumentException.class, () -> personService.create(testPerson));
    }

    /**
     * Test of create with Role null method, of class PersonService.
     */
    @Test
    public void testCreateWithRoleNull() {
        testPerson = new Person(new Name("Minch"), new Name("Yoda"), 1877, null, new SalaryManager(LocalDateTime.of(2025, 04, 14, 00, 00, 00, 000000), 400, 500));
        Person expResult = testPerson;
        Person result = personService.create(testPerson);
        assertEquals(expResult, result);

        Person deletePerson = repository.findByTag(testPerson.getTag());

        personService.delete(deletePerson.getTag());

    }

    /**
     * Test of find method, of class PersonService.
     */
//    @Test
//    public void testCreateFirstUser() {
//        tearDown();
//
//        Person person = new Person(new Name("Anders"), new Name("Jackson"), 2005, Role.USER);
//        PersonDTO personDTO = new PersonDTO("Anders Jackson", 2005, Role.ADMINISTRATOR);
//
//        PersonDTO expResult = personDTO;
//        PersonDTO result = personService.create(person);
//        
//
//        assertEquals(expResult, result);
//        
//       
//
//    }
    /**
     * Test of delete method, of class PersonService.
     */
    @Test
    public void testDelete() {

        person = new Person(new Name("Minch"), new Name("Yoda"), 1877, Role.USER, new SalaryManager(LocalDateTime.of(2025, 04, 14, 00, 00, 00, 000000), 400, 500));
        personService.create(person);

        SimplePersonDTO expResult = mapper.toSimplePersonDto(person);
        SimplePersonDTO result = mapper.toSimplePersonDto(personService.delete(person.getTag()));

        assertEquals(expResult, result);

        Person faultyIdPerson = new Person(100000000L, new Name("Macke"), new Name("Döds"), 1967, Role.USER, new SalaryManager(LocalDateTime.of(2025, 04, 14, 00, 00, 00, 000000), 400, 500));
        assertThrows(EntityNotFoundException.class, () -> personService.delete(faultyIdPerson.getTag()));
    }
}
