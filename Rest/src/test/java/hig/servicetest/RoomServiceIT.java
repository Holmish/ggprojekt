/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.servicetest;

import hig.BasicApplication;
import hig.domain.Name;
import hig.domain.Room;
import hig.repository.CleaningRepository;
import hig.repository.RoomRepository;
import hig.service.exceptions.IllegalActionException;
import hig.service.room.RoomDTO;
import hig.service.room.RoomMapper;
import hig.service.room.RoomService;
import jakarta.persistence.EntityNotFoundException;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author 21emho01
 */
@TestPropertySource(locations = "classpath:test.application.properties")
@SpringBootTest(classes = BasicApplication.class)
@ExtendWith(SpringExtension.class)
public class RoomServiceIT {

    @Autowired
    private RoomService roomService;

    @Autowired
    private CleaningRepository cleaningRepository;

    @Autowired
    private RoomMapper roomMapper;

    @Autowired
    private RoomRepository roomRepository;

    public RoomServiceIT() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of find method, of class RoomService.
     */
    @Test
    public void testFind() {
        System.out.println("find");
        Long id = 1L;
        RoomDTO expResult = new RoomDTO("Superrummet", "Superkul rum");
        RoomDTO result = roomService.find(id);

        assertEquals(expResult, result);
    }

    /**
     * Test of findAll method, of class RoomService.
     */
    @Test
    public void testFindAll() {
        System.out.println("findAll");

        List<RoomDTO> expResult = List.of(new RoomDTO("Superrummet", "Superkul rum"), new RoomDTO("mAx Burgers", "mAx, dåliga burgare och ej marknadspriser"), new RoomDTO("Häktet", "Jobbig Sixten..."));
        List<RoomDTO> result = roomService.findAll();
        assertEquals(expResult, result);

    }

    /**
     * Test of create method, of class RoomService.
     */
    @Test
    public void testCreate() {
        System.out.println("create");
        Room expResult = new Room(new Name("The white house"), "Bor någon trött gammal och demente gubbe här");
        Room result = roomService.create(expResult);
        assertEquals(expResult, result);
        roomRepository.delete(expResult);
    }

    /**
     * Test of createNameNull method, of class RoomService.
     */
    @Test
    public void testCreateNameNull() {
        System.out.println("createNameNull");

        Room roomWithoutName = new Room(null, "Bor någon trött gammal och dement gubbe här");
        assertThrows(IllegalArgumentException.class, () -> roomService.create(roomWithoutName));
    }

    /**
     * Test of delete method, of class RoomService.
     */
    @Test
    public void testDelete() {
        System.out.println("delete");
        Room room = new Room(new Name("Häktet"), "Jobbig Sixten...");
        Room createRoom = roomService.create(room);
        RoomDTO expResult = roomMapper.toRoomDto(createRoom);
        RoomDTO result = roomService.delete(createRoom.getId());
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteIdDontExist method, of class RoomService.
     */
    public void testDeleteIdDontExist() {
        assertThrows(EntityNotFoundException.class, () -> roomService.delete(100L));
    }

    /**
     * Test of deleteRoomWithCleanings method, of class RoomService.
     */
    public void testDeleteRoomWithCleanings() {
        assertThrows(IllegalActionException.class, () -> roomService.delete(2L));
    }

}
