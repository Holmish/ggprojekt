/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.rest;

import hig.BasicApplication;
import hig.domain.Name;
import hig.domain.Person;
import hig.domain.Role;
import hig.service.person.PersonService;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.http.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author 21masu04
 */
@SpringBootTest(classes = BasicApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:test.application.properties")
public class PersonControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private PersonService personService;

    @LocalServerPort
    private int port;

    private String getRootUrl() {
        return "http://localhost:" + port;
    }

    public PersonControllerIT() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of createPersonWithAdminRights method, of class PersonController.
     */
    @Test
    public void testCreatePersonWithAdminRights() {
        System.out.println("createPersonWithAdminRights");

        String url = getRootUrl() + "/Person/Create";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("tag", "7a3b09b9-e56c-49fb-a1d3-7fb9d61efabc");

        Person newPerson = new Person(new Name("Kalix"),
                new Name("Killen"), 1989, Role.USER, null);

        HttpEntity<Person> request = new HttpEntity<>(newPerson, headers);
        ResponseEntity<Person> response = restTemplate.postForEntity(url, request, Person.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Kalix Killen", response.getBody().getFullName());
        assertEquals(1989, response.getBody().getBirthyear());
        assertEquals(Role.USER, response.getBody().getRole());

        personService.delete(newPerson.getTag());
    }

    /**
     * Test of createPersonWithoutAdminRights method, of class PersonController.
     */
    @Test
    public void testCreatePersonWithoutAdminRights() {
        System.out.println("testCreatePersonWithoutAdminRights");
        Person user = new Person(new Name("Rudolf"), new Name("Andersson"),
                1967, Role.USER, null);
        personService.create(user);

        String url = getRootUrl() + "/Person/Create";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("tag", user.getTag());

        Person newPerson = new Person(new Name("Kalix"),
                new Name("Killen"), 1989, Role.USER, null);

        HttpEntity<Person> request = new HttpEntity<>(newPerson, headers);
        ResponseEntity<Person> response = restTemplate.postForEntity(url, request, Person.class);

        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        personService.delete(user.getTag());
    }

    /**
     * Test of deleteWithAdminRights method, of class PersonController.
     */
    @Test
    public void testDeleteWithAdminRights() {
        System.out.println("testDeleteWithAdminRights");

        Person newPerson = new Person(new Name("Kiruna"),
                new Name("Kingen"), 1999, Role.USER, null);
        personService.create(newPerson);

        String url = getRootUrl() + "/Person/Delete";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("callerTag", "7a3b09b9-e56c-49fb-a1d3-7fb9d61efabc");
        headers.set("targetTag", newPerson.getTag());

        HttpEntity<Person> request = new HttpEntity<>(headers);
        ResponseEntity<Person> response = restTemplate.postForEntity(url, request, Person.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Kiruna Kingen", response.getBody().getFullName());
        assertEquals(1999, response.getBody().getBirthyear());
        assertEquals(Role.USER, response.getBody().getRole());

    }

    /**
     * Test of deleteWithoutadminRights method, of class PersonController.
     */
    @Test
    public void testDeleteWithoutAdminRights() {
        System.out.println("testDeleteWithoutAdminRights");

        Person user = new Person(new Name("Rudolf"), new Name("Andersson"),
                1967, Role.USER, null);
        personService.create(user);

        Person newPerson = new Person(new Name("Kiruna"),
                new Name("Kingen"), 1999, Role.USER, null);
        personService.create(newPerson);

        String url = getRootUrl() + "/Person/Delete";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("callerTag", user.getTag());
        headers.set("targetTag", newPerson.getTag());

        HttpEntity<Person> request = new HttpEntity<>(headers);
        ResponseEntity<Person> response = restTemplate.postForEntity(url, request, Person.class);

        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());

        personService.delete(newPerson.getTag());
        personService.delete(user.getTag());
    }

    /**
     * Test of deleteYourself method, of class PersonController.
     */
    @Test
    public void testDeleteYourself() {
        System.out.println("testDeleteYourself");
        Person admin = new Person(new Name("Rudolf"), new Name("Andersson"),
                1967, Role.ADMINISTRATOR, null);
        personService.create(admin);

        String url = getRootUrl() + "/Person/Delete";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("callerTag", admin.getTag());
        headers.set("targetTag", admin.getTag());

        HttpEntity<Person> request = new HttpEntity<>(headers);
        ResponseEntity<Person> response = restTemplate.postForEntity(url, request, Person.class);

        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());

        personService.delete(admin.getTag());

    }

}
