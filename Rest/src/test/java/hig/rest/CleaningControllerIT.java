/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.rest;

import hig.BasicApplication;
import hig.domain.Cleaning;
import hig.domain.Name;
import hig.domain.Person;
import hig.domain.Role;
import hig.service.cleaning.CleaningDTO;
import hig.service.cleaning.CleaningService;
import hig.service.person.PersonService;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author 21masu04
 */
@SpringBootTest(classes = BasicApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:test.application.properties")
public class CleaningControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private CleaningService cleaningService;

    @Autowired
    private PersonService personService;

    @LocalServerPort
    private int port;

    private String getRootUrl() {
        return "http://localhost:" + port;
    }

    public CleaningControllerIT() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of createWithValidTag method, of class CleaningController.
     */
    @Test
    public void testCreateWithValidTag() {
        System.out.println("testCreateWithValidTag");

        Long roomId = 2L;
        String url = getRootUrl() + "/Cleaning/" + roomId;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("tag", "testTag");

        HttpEntity<Cleaning> request = new HttpEntity<>(headers);
        ResponseEntity<CleaningDTO> response = restTemplate.postForEntity(url, request, CleaningDTO.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Test Persson", response.getBody().person().fullName());
        assertEquals("mAx Burgers", response.getBody().room().roomName());
    }

    /**
     * Test of createWithInvalidTag method, of class CleaningController.
     */
    @Test
    public void testCreateWithInvalidTag() {
        System.out.println("testCreateWithInvalidTag");

        Long roomId = 2L;
        String url = getRootUrl() + "/Cleaning/" + roomId;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("tag", "lalala");

        HttpEntity<Cleaning> request = new HttpEntity<>(headers);
        ResponseEntity<CleaningDTO> response = restTemplate.postForEntity(url, request, CleaningDTO.class);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    /**
     * Test of createWithInvalidTag method, of class CleaningController.
     */
    @Test
    public void testCreateWithNoTag() {
        System.out.println("testCreateWithNoTag");

        Long roomId = 2L;
        String url = getRootUrl() + "/Cleaning/" + roomId;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<Cleaning> request = new HttpEntity<>(headers);
        ResponseEntity<CleaningDTO> response = restTemplate.postForEntity(url, request, CleaningDTO.class);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    /**
     * Test of getByPersonIdAndInterval method, of class CleaningController.
     */
    @Test
    public void testGetByPersonIdAndInterval() {
        System.out.println("testGetByPersonIdAndInterval");
        LocalDate fromDate = LocalDate.of(2024, 05, 11);
        LocalDate toDate = null;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(getRootUrl() + "/Cleaning")
                .queryParam("from", fromDate)
                .queryParam("to", toDate);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("targetTag", "testTag");
        headers.set("callerTag", "7a3b09b9-e56c-49fb-a1d3-7fb9d61efabc");

        HttpEntity<Void> request = new HttpEntity<>(headers);

        ResponseEntity<List<CleaningDTO>> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                request,
                new ParameterizedTypeReference<List<CleaningDTO>>() {},
                fromDate,
                toDate);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertFalse(response.getBody().size() == 0);
    }

    /**
     * Test of GetByPersonIdAndIntervalFromYourselfAsUser method, of class
     * CleaningController.
     */
    @Test
    public void testGetByPersonIdAndIntervalFromYourselfAsUser() {
        System.out.println("testGetByPersonIdAndIntervalFromYourselfAsUser");
        Person user = new Person(new Name("Rudolf"), new Name("Andersson"),
                1967, Role.USER, null);
        personService.create(user);

        LocalDate fromDate = LocalDate.of(2024, 05, 11);
        LocalDate toDate = null;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(getRootUrl() + "/Cleaning")
                .queryParam("from", fromDate)
                .queryParam("to", toDate);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("targetTag", user.getTag());
        headers.set("callerTag", user.getTag());

        HttpEntity<Void> request = new HttpEntity<>(headers);

        ResponseEntity<List<CleaningDTO>> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                request,
                new ParameterizedTypeReference<List<CleaningDTO>>() {},
                fromDate,
                toDate);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(response.getBody().size() == 0);

        personService.delete(user.getTag());
    }

    /**
     * Test of GetByPersonIdAndIntervalFromSomeoneElseAsUser method, of class
     * CleaningController.
     */
    @Test
    public void testGetByPersonIdAndIntervalFromSomeoneElseAsUser() {
        System.out.println("testGetByPersonIdAndIntervalFromSomeoneElseAsUser");
        Person user = new Person(new Name("Rudolf"), new Name("Andersson"),
                1967, Role.USER, null);
        personService.create(user);

        LocalDate fromDate = LocalDate.of(2024, 05, 11);
        LocalDate toDate = null;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(getRootUrl() + "/Cleaning")
                .queryParam("from", fromDate)
                .queryParam("to", toDate);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("targetTag", "7a3b09b9-e56c-49fb-a1d3-7fb9d61efabc");
        headers.set("callerTag", user.getTag());

        HttpEntity<Void> request = new HttpEntity<>(headers);

        ResponseEntity<CleaningDTO> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                request,
                new ParameterizedTypeReference<CleaningDTO>() {},
                fromDate,
                toDate);

        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());

        personService.delete(user.getTag());
    }

}
