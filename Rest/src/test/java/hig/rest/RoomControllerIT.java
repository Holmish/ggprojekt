/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.rest;

import hig.BasicApplication;
import hig.domain.Name;
import hig.domain.Person;
import hig.domain.Role;
import hig.domain.Room;
import hig.service.person.PersonService;
import hig.service.room.RoomService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author 21sida01
 */
@SpringBootTest(classes = BasicApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = "classpath:test.application.properties")
public class RoomControllerIT {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private RoomService roomService;
    
    @Autowired
    private PersonService personService;

    @LocalServerPort
    private int port;

    private String getRootUrl() {
        return "http://localhost:" + port;
    }

    public RoomControllerIT() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    
    /**
     * Test of createWithAdminRights method, of class RoomController.
     */
    @Test
    public void testCreateWithAdminRights() {
        System.out.println("testCreateWithAdminRights");
        
        String url = getRootUrl() + "/Room/Create";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("tag", "7a3b09b9-e56c-49fb-a1d3-7fb9d61efabc");
        
        
        Room newRoom = new Room(new Name("Max mullvadshåla"), "svettigt..");
         HttpEntity<Room> request = new HttpEntity<>(newRoom, headers);
        ResponseEntity<Room> response = restTemplate.postForEntity(url, request, Room.class);
        
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Max mullvadshåla", response.getBody().getName().getName());
        assertEquals("svettigt..", response.getBody().getComment());
        
        roomService.delete(response.getBody().getId());
    }
    
    /**
     * Test of createWithAdminRights method, of class RoomController.
     */
    @Test
    public void testCreateWithoutAdminRights() {
        System.out.println("testCreateWithAdminRights");
        
        Person person = new Person(new Name("Benny"),
                new Name("Doffman"), 1999, Role.USER, null);
        
        personService.create(person);
        
        String url = getRootUrl() + "/Room/Create";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("tag", person.getTag());
        
        Room newRoom = new Room(new Name("Max mullvadshåla"), "svettigt..");
        HttpEntity<Room> request = new HttpEntity<>(newRoom, headers);
        ResponseEntity<Room> response = restTemplate.postForEntity(url, request, Room.class);
        
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        
        personService.delete(person.getTag());
    }
}
