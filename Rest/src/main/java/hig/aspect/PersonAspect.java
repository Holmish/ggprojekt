/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.aspect;

import hig.domain.Person;
import hig.domain.Role;
import hig.repository.PersonRepository;
import jakarta.servlet.http.HttpServletRequest;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author 21masu04
 */
@Aspect
@Component
public class PersonAspect {

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private PersonRepository personRepository;

    @Before("execution(* hig.rest.PersonController.create(..))")
    public void checkCreateAuthorization(JoinPoint joinPoint) {
        try {

            Optional<String> tag = Optional.ofNullable(request.getHeader("callerTag"));
            if (personRepository.findAll().isEmpty()) {
                // If the database is empty no tag should exist
                if (!tag.isEmpty()) {
                    throw new SecurityException("Permission to create user denied.");
                }
                Object[] args = joinPoint.getArgs();
                Person person = (Person) args[0];
                person.setRole(Role.ADMINISTRATOR);
            } else {
                checkAdminRights();
            }
        } catch (NullPointerException e) {
            throw new SecurityException("Invalid input");
        } catch (NoSuchElementException e) {
            throw new SecurityException("Incorrect authorization");
        }

    }

    @Before("execution(* hig.rest.PersonController.updateSalary(..))")
    public void checkUpdateSalaryAuthorization() {

        checkAdminRights();

    }

    @Before("execution(* hig.rest.PersonController.delete(..))")
    public void checkDeleteAuthorization() {

        checkAdminRights();

        String targetTag = Optional.ofNullable(request.getHeader("targetTag")).orElseThrow(() -> new SecurityException("Invalid input"));
        String callerTag = Optional.ofNullable(request.getHeader("callerTag")).get();

        if (callerTag.equals(personRepository.findByTag(targetTag).getTag())) {
            throw new SecurityException("Administrators cannot delete themselves");
        }

    }

    public void checkAdminRights() {

        try {

            String callerTag = Optional.ofNullable(request.getHeader("callerTag")).get();
            Person person = personRepository.findByTag(callerTag);
            if (person.getRole() != Role.ADMINISTRATOR) {
                throw new SecurityException("Missing admin rights");
            }
        } catch (NullPointerException e) {
            throw new SecurityException("Invalid input");
        } catch (NoSuchElementException e) {
            throw new SecurityException("Access denied");

        }

    }

}
