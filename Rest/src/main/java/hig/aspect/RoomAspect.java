/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.aspect;

import hig.domain.Person;
import hig.domain.Role;
import hig.repository.PersonRepository;
import jakarta.servlet.http.HttpServletRequest;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author 21masu04
 */
@Aspect
@Component
public class RoomAspect {
    
    @Autowired
    private HttpServletRequest request;
    
    @Autowired
    private PersonRepository repository;
    
    @Before("execution(* hig.rest.RoomController.create(..))")
    public void checkCreateAuthorization() {
        try {
            Optional<String> tag = Optional.ofNullable(request.getHeader("tag"));
            Person person = repository.findByTag(tag.get());

            if (person.getRole() == Role.USER) {
                throw new SecurityException("Access denied");
            }

        } catch (NoSuchElementException e) {
            throw new SecurityException("Access denied");
        } catch (NullPointerException e) {
            throw new SecurityException("Invalid input");
        }

    }
}
