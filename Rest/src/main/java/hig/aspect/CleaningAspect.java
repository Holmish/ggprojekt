/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.aspect;

import hig.domain.Person;
import hig.domain.Role;
import hig.repository.PersonRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author 21masu04
 */
@Aspect
@Component
public class CleaningAspect {

    @Autowired
    private HttpServletRequest request;
    
    @Autowired
    private PersonRepository personRepository;

    @Before("execution(* hig.rest.CleaningController.create(..))")
    public void checkCreateAuthorization() {
        try {
            String tag = Optional.ofNullable(request.getHeader("tag")).get();
            Person personCaller = personRepository.findByTag(tag);

            if (personCaller == null) {
                throw new EntityNotFoundException("Invalid tag");
            }

        } catch (NoSuchElementException e) {
            throw new NoSuchElementException("No tag found");
        }
    }

    @Before("execution(* hig.rest.CleaningController.getByPersonIdAndInterval(..))")
    public void checkRole() {

        String callerTag = Optional.ofNullable(request.getHeader("callerTag")).get();
        String targetTag = Optional.ofNullable(request.getHeader("targetTag")).get();
        Person personCalling = personRepository.findByTag(callerTag);
        Person personTarget = personRepository.findByTag(targetTag);

        if (personCalling.getRole().equals(Role.USER)) {
            if (!personTarget.getTag().equals(callerTag)) {
                throw new SecurityException("Can't check other users cleanings without admin rights");
            }
        }
    }

}
