package hig.exception;

import hig.service.exceptions.IllegalActionException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 *
 * @author 21masu01
 */
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private HttpServletRequest request;

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");

    private final Map<Class, HttpStatus> validCodes = Map.of(
            SecurityException.class, HttpStatus.FORBIDDEN,
            IllegalArgumentException.class, HttpStatus.FORBIDDEN,
            NullPointerException.class, HttpStatus.BAD_REQUEST,
            EntityNotFoundException.class, HttpStatus.NOT_FOUND,
            NoSuchElementException.class, HttpStatus.NOT_FOUND,
            IllegalActionException.class, HttpStatus.FORBIDDEN);

    @ExceptionHandler(value = {RuntimeException.class})
    public ResponseEntity<ExceptionWrapper> handleRuntimeExcpetion(RuntimeException ex) {
        return new ResponseEntity<>(new ExceptionWrapper(
                ex.getClass().getSimpleName(),
                ex.getMessage(),
                formatter.format(LocalDateTime.now()),
                request.getServletPath()),
                getStatus(ex));
    }

    private HttpStatus getStatus(RuntimeException cause) {
        return validCodes.getOrDefault(cause.getClass(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static record ExceptionWrapper(String type, String message, String timestamp, String path) {

    }
}
