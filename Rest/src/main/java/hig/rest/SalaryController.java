/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.rest;

import hig.domain.Salary;
import hig.service.salary.SalaryDTO;
import hig.service.salary.SalaryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author 22emfa02
 */
@RestController
@RequestMapping("Salary")
public class SalaryController {
    
    private final SalaryService service;
    
    @Autowired
    public SalaryController(SalaryService service) {
        this.service = service;
    }
    
    @GetMapping("/Find")
    public SalaryDTO find(@RequestHeader Long id) {
        return service.find(id);
    }
    
    @GetMapping
    public List<SalaryDTO> findAll() {
        return service.findAll();
    }
    
    @PostMapping("/Create") 
    public Salary create (@RequestHeader Long personId) {
        return service.create(personId);
    }
    

    
    
    
}
