package hig.rest;

import hig.domain.Room;
import hig.service.room.RoomDTO;
import hig.service.room.RoomService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author 22emfa02
 */
@RestController
@RequestMapping("Room")
public class RoomController {

    private final RoomService service;

    @Autowired
    public RoomController(RoomService service) {
        this.service = service;
    }

    @GetMapping
    public List<RoomDTO> findAll() {
        return service.findAll();
    }

    @GetMapping("/Find")
    public RoomDTO find(@RequestHeader Long id) {
        return service.find(id);
    }

    @PostMapping("/Create")
    public Room create(@RequestBody Room room) {
        return service.create(room);
    }

    @PostMapping("/Delete/{roomId}")
    public RoomDTO delete(@PathVariable Long roomId) {
        return service.delete(roomId);
    }

}
