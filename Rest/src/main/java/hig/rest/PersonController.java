package hig.rest;

import hig.service.person.PersonService;
import hig.domain.Person;
import hig.service.person.PersonDTO;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author Grymma Gittarna
 */
@RestController
@RequestMapping("Person")
public class PersonController {

    private final PersonService service;

    @Autowired
    public PersonController(PersonService service) {
        this.service = service;
    }

    @GetMapping
    public List<PersonDTO> findAll(@RequestHeader String callerTag) {

        return service.findAll(callerTag);

    }

    @GetMapping("/Find")
    public PersonDTO find(@RequestHeader String targetTag, @RequestHeader String callerTag) {

        return service.find(targetTag, callerTag);
    }

    @PostMapping("/Create")
    public Person create(@RequestBody Person person) {
        return service.create(person);
    }

    @PostMapping("/Delete")
    public Person delete(@RequestHeader String callerTag, @RequestHeader String targetTag) {
        return service.delete(targetTag);
    }

    @PostMapping("/UpdateSalary")
    public int updateSalary(@RequestHeader String targetTag, @RequestHeader Integer newSalary, @RequestHeader LocalDate dateOfChange) {
        LocalDateTime newSalaryDate = LocalDateTime.of(dateOfChange.getYear(), dateOfChange.getMonthValue(), dateOfChange.getDayOfMonth(), 0, 0);
        return service.updateSalary(targetTag, newSalary, newSalaryDate);
    }
}
