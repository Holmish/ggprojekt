/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.rest;

import hig.service.cleaning.CleaningDTO;
import hig.service.cleaning.CleaningService;
import java.time.LocalDateTime;
import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author EMELIE YOURS TRULY THE ONE AND ONLY
 */
@RestController
@RequestMapping("Cleaning")
public class CleaningController {

    private final CleaningService service;

    @Autowired
    public CleaningController(CleaningService service) {
        this.service = service;
    }

    @PostMapping("{roomId}")
    public CleaningDTO create(@RequestHeader(required = false) String tag, @PathVariable Long roomId) {
        return service.create(roomId, tag);
    }

    @GetMapping("{roomId}")
    public List<CleaningDTO> findByRoomId(@PathVariable Long roomId) {
        System.out.println("CleaningController getByRoomId");
        return service.findByRoomId(roomId);
    }

    @GetMapping("{roomId}/{days}")
    public List<CleaningDTO> findByRoomIdAndDays(@PathVariable Long roomId, @PathVariable Long days) {
        return service.findByRoomIdAndDays(roomId, days);
    }

    @GetMapping()
    public List<CleaningDTO> getByPersonIdAndInterval(@RequestHeader String targetTag,
            @RequestParam(value = "from", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate from,
            @RequestParam(value = "to", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate to) {
        LocalDateTime start = null;
        if (from != null) {
            start = LocalDateTime.of(from.getYear(), from.getMonthValue(), from.getDayOfMonth(), 0, 0);
        }
        LocalDateTime end = null;
        if (to != null) {
            end = LocalDateTime.of(to.getYear(), to.getMonthValue(), to.getDayOfMonth(), 23, 59);
        }
        return service.findByPersonIdAndInterval(targetTag, start, end);
    }
}
