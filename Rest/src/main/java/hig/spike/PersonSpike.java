/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.spike;

import hig.domain.Name;
import hig.domain.Person;
import hig.domain.Role;
import hig.domain.SalaryManager;
import java.time.LocalDateTime;

/**
 *
 * @author 21emho01
 */
public class PersonSpike {

    public static void main(String[] args) {
        Person testPerson = new Person(new Name("Torsten"), new Name("Flink"), 1978, Role.ADMINISTRATOR, new SalaryManager(LocalDateTime.of(2025,04,14,00,00,00,000000), 400, 500));

        System.out.println(testPerson.getFirstName());
        System.out.println(testPerson.getLastName());
        System.out.println(testPerson.getFullName());
        System.out.println(testPerson.getBirthyear());
        System.out.println(testPerson.getRole());
        System.out.println(testPerson.getTag());
    }

}
