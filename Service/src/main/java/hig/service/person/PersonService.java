package hig.service.person;

import hig.domain.Person;
import hig.domain.Role;
import hig.domain.Salary;
import hig.domain.SalaryManager;
import hig.repository.*;
import jakarta.persistence.EntityNotFoundException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jakarta.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 *
 * @author GG
 */
@Service
public class PersonService {

    private final PersonRepository repository;
    private final PersonMapper mapper;
    private final SalaryRepository salaryRepository;

    @Autowired
    public PersonService(
            PersonRepository repository,
            PersonMapper mapper, SalaryRepository salaryRepository, HttpServletRequest request) {
        this.repository = repository;
        this.mapper = mapper;
        this.salaryRepository = salaryRepository;
    }

    public PersonDTO find(String targetTag, String callerTag) {
        return mapper.toPersonDto(checkIfPersonNull(targetTag), repository.findByTag(callerTag));
    }

    public List<PersonDTO> findAll(String callerTag) {
        return mapper.toPersonDtoList(repository.findAll(), repository.findByTag(callerTag));
    }

    public Person create(Person person) {

        if (person.getFirstName() == null || person.getLastName() == null) {

            throw new IllegalArgumentException("Name cannot be null.");
        }

        if (person.getRole() == null) {
            person.setRole(Role.USER);
        }
        return repository.save(person);
    }

    public Person delete(String targetTag) {

        Person person = checkIfPersonNull(targetTag);

        repository.delete(person);

        return person;
    }

    public int updateSalary(String targetTag, Integer newSalary, LocalDateTime dateOfChange) {
        Person person = checkIfPersonNull(targetTag);

        Integer currentSalary = Optional.ofNullable(person)
                .map(Person::getSalary)
                .map(s -> s.getCurrentSalary())
                .orElse(0);

        SalaryManager salaryManager = new SalaryManager(dateOfChange, currentSalary, newSalary);
        Optional<LocalDateTime> lastPayoutOptional = salaryRepository.findLatestPaymentDateByPersonId(person.getId());
        if (lastPayoutOptional.isEmpty()) {
            return repository.updateSalary(targetTag, salaryManager.getCurrentSalary(), salaryManager.getUpcomingSalary(), dateOfChange);
        }
        if (dateOfChange.isBefore(lastPayoutOptional.get())) {
            throw new IllegalArgumentException("Given change date for salary cannot be before the latest payout.");
        }

        return repository.updateSalary(targetTag, salaryManager.getCurrentSalary(), salaryManager.getUpcomingSalary(), dateOfChange);
    }

    private Person checkIfPersonNull(String tag) {
        Person person = repository.findByTag(tag);

        if (person == null) {
            throw new EntityNotFoundException("The person does not exist");
        }

        return person;
    }

}
