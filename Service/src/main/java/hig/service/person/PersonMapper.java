/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package hig.service.person;

import hig.domain.Person;
import java.util.List;
import java.util.stream.Collectors;
import org.mapstruct.*;
import org.springframework.stereotype.Component;

/**
 *
 * @author 21emho01
 */
@Component
@Mapper(componentModel = "spring")
public interface PersonMapper {

    default PersonDTO toPersonDto(Person person, Person currentUser) {
        return PersonDTOFactory.createPersonDTO(person, currentUser);
    }
    
    SimplePersonDTO toSimplePersonDto(Person person);
        

    default List<PersonDTO> toPersonDtoList(List<Person> persons, Person currentUser) {
        return persons.stream()
                .map(person -> toPersonDto(person, currentUser))
                .collect(Collectors.toList());
    }

    
}
