/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Record.java to edit this template
 */
package hig.service.person;

import hig.domain.Role;

/**
 *
 * @author 21emho01
 */
public record SimplePersonDTO(String fullName, Integer birthyear, Role role) implements PersonDTO {

}
