/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.service.person;

import hig.domain.Person;

/**
 *
 * @author 21emho01
 */
public class PersonDTOFactory {

    public static PersonDTO createPersonDTO(Person person, Person currentUser) {

        switch (currentUser.getRole()) {
            case ADMINISTRATOR -> {
                return new AdvancedPersonDTO(person.getFullName(),
                        person.getBirthyear(), person.getRole(), person.getTag());
            }
            case USER -> {
                return new SimplePersonDTO(person.getFullName(),
                        person.getBirthyear(), person.getRole());
            }
            default ->
                throw new IllegalArgumentException("Invalid role");
        }

    }

}
