package hig.service.cleaning;

import hig.service.person.SimplePersonDTO;
import hig.service.room.RoomDTO;
import java.time.LocalDateTime;

/**
 *
 * @author 22emfa02
 */
public record CleaningDTO(RoomDTO room, SimplePersonDTO person, LocalDateTime time) {

}
