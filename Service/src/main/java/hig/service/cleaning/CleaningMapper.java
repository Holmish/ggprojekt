/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.service.cleaning;

import hig.domain.Cleaning;
import hig.service.person.PersonMapper;
import java.util.List;
import java.util.stream.Collectors;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

/**
 *
 * @author 22emfa02
 */
@Component
@Mapper(componentModel = "spring", uses = PersonMapper.class)
public interface CleaningMapper {

    @Mapping(source = "room.stringName", target = "room.roomName")
    CleaningDTO toCleaningDto(Cleaning cleaning);

    default List<CleaningDTO> toCleaningDtoList(List<Cleaning> cleanings) {
        return cleanings.stream()
                .map(this::toCleaningDto)
                .collect(Collectors.toList());
    }
}
