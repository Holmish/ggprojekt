package hig.service.cleaning;

import hig.domain.Cleaning;
import hig.domain.Person;
import hig.domain.Room;
import hig.repository.CleaningRepository;
import hig.repository.PersonRepository;
import hig.repository.RoomRepository;
import jakarta.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author 22emfa02
 */
@Service
public class CleaningService {

    private final CleaningRepository repository;
    private final PersonRepository personRepository;
    private final RoomRepository roomRepository;
    private final CleaningMapper mapper;
    
    @Autowired
    public CleaningService(CleaningRepository repository, PersonRepository personRepository, RoomRepository roomRepository, CleaningMapper mapper) {
        this.repository = repository;
        this.personRepository = personRepository;
        this.roomRepository = roomRepository;
        this.mapper = mapper;
    }

    public List<CleaningDTO> findByRoomId(Long roomId) {
        LocalDateTime today = LocalDateTime.now();
        return mapper.toCleaningDtoList(repository.findByRoomIdAndTimeGreaterThan(roomId, LocalDateTime.of(today.getYear(), today.getMonthValue(), today.getDayOfMonth(), 0, 0)));
    }

    public List<CleaningDTO> findByRoomIdAndDays(Long roomId, Long days) {
        if (days < 0) {
            throw new IllegalArgumentException("You cannot enter a negative amount of days");
        }
        return mapper.toCleaningDtoList(repository.findByRoomIdAndTimeGreaterThan(roomId, LocalDateTime.now().minusDays(days)));
    }

    public List<CleaningDTO> findByPersonIdAndInterval(String tag, LocalDateTime from, LocalDateTime to) {

        Person person = personRepository.findByTag(tag);
        // Open interval
        if (from == null && to == null) {
            return mapper.toCleaningDtoList(repository.findByPersonId(person.getId()));
            // No start date   
        } else if (from == null && to != null) {
            return mapper.toCleaningDtoList(repository.findByPersonIdAndTimeLessThan(person.getId(), to));
            // No end date  
        } else if (from != null && to == null) {
            return mapper.toCleaningDtoList(repository.findByPersonIdAndTimeGreaterThan(person.getId(), from));
            // Closed interval  
        } else {
            return mapper.toCleaningDtoList(repository.findByPersonIdAndTimeGreaterThanAndTimeLessThan(person.getId(), from, to));
        }
    }

    public CleaningDTO create(Long roomId, String tag) {
        Person person = personRepository.findByTag(tag);
        Room room = roomRepository.findById(roomId).orElseThrow(() -> new EntityNotFoundException("No room with given id found"));
        return mapper.toCleaningDto(repository.save(new Cleaning(room, person)));
    }

}
