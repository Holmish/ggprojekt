/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.service.exceptions;

/**
 *
 * @author 21emho01
 */
public class IllegalActionException extends RuntimeException {
    
    public IllegalActionException(String message) {
        super(message);
    }
    
}
