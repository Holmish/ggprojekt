/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.service.room;

import hig.domain.Cleaning;
import hig.domain.Room;
import hig.repository.CleaningRepository;
import hig.repository.RoomRepository;
import jakarta.persistence.EntityNotFoundException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import hig.service.exceptions.IllegalActionException;

/**
 *
 * @author 22emfa02
 */
@Service
public class RoomService {

    private final RoomRepository roomRepository;
    private final RoomMapper mapper;
    private final CleaningRepository cleaningRepository;

    @Autowired
    public RoomService(RoomMapper mapper, RoomRepository repository, CleaningRepository cleaningRepository) {
        this.roomRepository = repository;
        this.mapper = mapper;
        this.cleaningRepository = cleaningRepository;
    }

    public RoomDTO find(Long id) {
        return mapper.toRoomDto(roomRepository.findById(id).orElseThrow());
    }

    public List<RoomDTO> findAll() {
        return mapper.toRoomDtoList(roomRepository.findAll());
    }

    public Room create(Room room) {
        if (room.getName() == null) {
            throw new IllegalArgumentException("Null cannot be named");
        }

        return roomRepository.save(room);
    }

    public RoomDTO delete(Long roomId) {

        RoomDTO roomToBeDeleted = mapper.toRoomDto(roomRepository.findById(roomId).orElseThrow(() -> new EntityNotFoundException("Room with given id does not exist")));
        List<Cleaning> roomToBeDeletedCleanings = cleaningRepository.findByRoomId(roomId);

        if (roomToBeDeletedCleanings.isEmpty()) {
            roomRepository.deleteById(roomId);
            return roomToBeDeleted;
        } else {
            throw new IllegalActionException("Can't delete room with registered cleanings");
        }
    }

}
