/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package hig.service.room;

import hig.domain.Room;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

/**
 *
 * @author 22emfa02
 */
@Component
@Mapper(componentModel = "spring")
public interface RoomMapper {

    @Mapping(target = "roomName", source = "stringName")
    RoomDTO toRoomDto(Room room);

    List<RoomDTO> toRoomDtoList(List<Room> room);

}
