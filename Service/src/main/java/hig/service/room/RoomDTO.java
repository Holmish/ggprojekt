/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.service.room;

/**
 *
 * @author 22sida01
 */
public record RoomDTO(String roomName, String comment) {

}
