/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.service.salary;

import hig.domain.Cleaning;
import hig.domain.Person;
import hig.domain.Salary;
import hig.repository.CleaningRepository;
import hig.repository.PersonRepository;
import hig.repository.SalaryRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author 21emho01
 */
@Service
public class SalaryService {

    private final SalaryRepository repository;
    private final PersonRepository personRepository;
    private final CleaningRepository cleaningRepository;
    private final SalaryMapper mapper;

    @Autowired
    public SalaryService(SalaryRepository repository, PersonRepository personRepository, CleaningRepository cleaningRepository, SalaryMapper mapper) {
        this.repository = repository;
        this.personRepository = personRepository;
        this.cleaningRepository = cleaningRepository;
        this.mapper = mapper;
    }

    public SalaryDTO find(Long id) {
        Salary salary = repository.findById(id).orElseThrow();
        return mapper.toSalaryDto(salary);
    }

    public List<SalaryDTO> findAll() {
        List<Salary> salaryList = repository.findAll();
        return mapper.toSalaryDtoList(salaryList);
    }

    public Salary create(Long personId) {

        List<Cleaning> cleaningsAfterSalaryChange;
        int totalAmount;
        Person person = personRepository.findById(personId).orElseThrow();
        Optional<LocalDateTime> lastPayoutOptional = repository.findLatestPaymentDateByPersonId(personId);
        LocalDateTime lastPayout;

        // No last payouts exists for the specific user.
        if (lastPayoutOptional.isEmpty()) {
            lastPayout = LocalDateTime.of(1, 2, 1, 1, 1, 1, 1);

        } else {
            lastPayout = lastPayoutOptional.get();
        }

        boolean noNewSalary = person.getSalary().getDateOfNewSalary() == null;

        if (noNewSalary) {
            cleaningsAfterSalaryChange = cleaningRepository.findByPersonIdAndTimeGreaterThan(personId, lastPayout);
            totalAmount = cleaningsAfterSalaryChange.size() * person.getSalary().getCurrentSalary();
        } else {
            List<Cleaning> cleaningsBeforeSalaryChange = cleaningRepository.
                    findByPersonIdAndTimeGreaterThanAndTimeLessThan(personId, lastPayout, person.getSalary().getDateOfNewSalary());
            
            cleaningsAfterSalaryChange = cleaningRepository.findByPersonIdAndTimeGreaterThan(personId, person.getSalary().getDateOfNewSalary());
            
            totalAmount = cleaningsBeforeSalaryChange.size() * person.getSalary().getCurrentSalary();
            totalAmount += cleaningsAfterSalaryChange.size() * person.getSalary().getUpcomingSalary();
            person.getSalary().updateSalary();
        }
        //}
        return repository.save(new Salary(person, LocalDateTime.now().withSecond(0).withNano(0), totalAmount));
    }

}
