/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.service.salary;

import hig.domain.Salary;
import java.util.List;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

/**
 *
 * @author 21emho01
 */
@Component
@Mapper(componentModel = "spring")
public interface SalaryMapper {
    SalaryDTO toSalaryDto(Salary salary);
    List<SalaryDTO> toSalaryDtoList(List<Salary> salaries);
}
