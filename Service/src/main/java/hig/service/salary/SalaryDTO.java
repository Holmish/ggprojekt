/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.service.salary;

import hig.service.person.SimplePersonDTO;
import java.time.LocalDateTime;

/**
 *
 * @author 21emho01
 */
public record SalaryDTO(SimplePersonDTO person, LocalDateTime date, Integer salary){
    
}
