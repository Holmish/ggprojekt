/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.service.salary;

import hig.domain.Salary;
import hig.repository.CleaningRepository;
import hig.repository.PersonRepository;
import hig.repository.SalaryRepository;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author 21masu04
 */
public class SalaryServiceTest {
    
    private SalaryService salaryService;
    @Mock
    private SalaryRepository salaryRepositoryMock;
    @Mock
    private PersonRepository personRepositoryMock;
    @Mock
    private CleaningRepository cleaningRepositoryMock;
    @Mock
    private SalaryMapper salaryMapperMock;
    @Mock
    private Salary salary;
    @Mock
    private Salary salary2;
    @Mock
    private SalaryDTO salaryDTO;
    @Mock
    private SalaryDTO salaryDTO2;
    
    public SalaryServiceTest() {
        MockitoAnnotations.initMocks(this);
        salaryService = new SalaryService(salaryRepositoryMock,personRepositoryMock
                ,cleaningRepositoryMock,salaryMapperMock);
        
    }
    
   
    /**
     * Test of find method, of class SalaryService.
     */
    @Test
    public void testFind() {
        System.out.println("find");
        when(salaryRepositoryMock.findById(1L)).thenReturn(Optional.of(salary));
        when(salaryMapperMock.toSalaryDto(salary)).thenReturn(salaryDTO);
        
        SalaryDTO expResult = salaryDTO;
        SalaryDTO result = salaryService.find(1L);
        
        assertEquals(expResult, result);
        verify(salaryRepositoryMock, times(1)).findById(1L);
        
        
        
    }

    /**
     * Test of findAll method, of class SalaryService.
     */
    @Test
    public void testFindAll() {
        
        List<Salary> salaryList =  List.of(salary, salary2);
        List<SalaryDTO> salaryDTOList = List.of(salaryDTO, salaryDTO2);
        
        System.out.println("findAll");
        when(salaryRepositoryMock.findAll()).thenReturn(salaryList);
        when(salaryMapperMock.toSalaryDtoList(salaryList)).thenReturn(salaryDTOList);
        
        List<SalaryDTO> expResult = salaryDTOList;
        List<SalaryDTO> result = salaryService.findAll();
        
        assertEquals(expResult, result);
        verify(salaryRepositoryMock, times(1)).findAll();
       
        
        
       
    }

    /**
     * Test of create method, of class SalaryService.
     */
    @Test
    public void testCreate() {
        System.out.println("create");
        
    }
    
}
