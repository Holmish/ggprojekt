package hig.service.person;

import hig.domain.Name;
import hig.domain.Person;
import hig.domain.Role;
import hig.domain.Salary;
import hig.domain.SalaryManager;
import hig.repository.PersonRepository;
import hig.repository.SalaryRepository;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import org.mockito.Mock;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author 21emho01
 */
public class PersonServiceTest {

    private PersonService personService;
    @Mock
    private PersonRepository personRepositoryMock;
    @Mock
    private SalaryRepository salaryRepository;
    @Mock
    private SalaryManager salaryManagerMock;
    @Mock
    private Salary salary;

    @Mock
    private PersonMapper personMapperMock;
    @Mock
    private HttpServletRequest requestMock;
    @Mock
    private Person person1;
    @Mock
    private Person person2;
    @Mock
    private PersonDTO person1DTO;
    @Mock
    private PersonDTO person2DTO;
    @Mock
    private Name nameMock;

    public PersonServiceTest() {
    }

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        personService = new PersonService(personRepositoryMock, personMapperMock, salaryRepository, requestMock);
    }

    @AfterEach
    public void tearDown() {
        personService = null;
    }

    /**
     * Test of find method, of class PersonService.
     */
    @Test
    public void testFind() {
        System.out.println("testFind");

        when(personRepositoryMock.findByTag(anyString())).thenReturn(person1);
        when(personMapperMock.toPersonDto(any(Person.class), any(Person.class))).thenReturn(person1DTO);
        PersonDTO expResult1 = person1DTO;
        PersonDTO result1 = personService.find("tag1", "tag2");

        assertEquals(expResult1, result1);

        when(personRepositoryMock.findByTag(anyString())).thenReturn(null);
        assertThrows(EntityNotFoundException.class, () -> personService.find("tag3", "tag4"));

        verify(personRepositoryMock, times(3)).findByTag(anyString());
        verify(personMapperMock, times(1)).toPersonDto(any(Person.class), any(Person.class));
    }

    /**
     * Test of findAll method, of class PersonService.
     */
    @Test
    public void testFindAll() {
        System.out.println("testFindAll");

        when(personRepositoryMock.findAll()).thenReturn(List.of(person1, person2));
        when(personRepositoryMock.findByTag("callerTag")).thenReturn(person1);
        when(personMapperMock.toPersonDtoList(List.of(person1, person2), person1)).thenReturn(List.of(person1DTO, person2DTO));

        List<PersonDTO> expResult = List.of(person1DTO, person2DTO);
        List<PersonDTO> result = personService.findAll("callerTag");
        assertEquals(expResult, result);

        verify(personRepositoryMock, times(1)).findAll();
        verify(personMapperMock, times(1)).toPersonDtoList(List.of(person1, person2), person1);
    }

    /**
     * Test of create method with both name present, of class PersonService.
     */
    @Test
    public void testCreateBothNamesPresent() {
        System.out.println("testCreateBothNamePresent");

        when(person1.getFirstName()).thenReturn(nameMock);
        when(person1.getLastName()).thenReturn(nameMock);
        when(person1.getRole()).thenReturn(Role.ADMINISTRATOR);
        when(personRepositoryMock.save(person1)).thenReturn(person1);

        Person expResult = person1;
        Person result = personService.create(person1);
        assertEquals(expResult, result);

        verify(person1, times(1)).getFirstName();
        verify(person1, times(1)).getLastName();
        verify(person1, times(1)).getRole();
        verify(personRepositoryMock, times(1)).save(person1);

    }

    /**
     * Test of create method when first name null, of class PersonService.
     */
    @Test
    public void testCreateFirstNameNull() {
        System.out.println("testCreateFirstNameNull");

        when(person1.getFirstName()).thenReturn(null);
        when(person1.getLastName()).thenReturn(nameMock);

        assertThrows(IllegalArgumentException.class, () -> personService.create(person1));

        verify(person1, times(1)).getFirstName();
        verify(person1, times(0)).getLastName();
    }

    /**
     * Test of create method when last name null, of class PersonService.
     */
    @Test
    public void testCreateLastNameNull() {
        System.out.println("testCreateLastNameNull");

        when(person1.getFirstName()).thenReturn(nameMock);
        when(person1.getLastName()).thenReturn(null);

        assertThrows(IllegalArgumentException.class, () -> personService.create(person1));

        verify(person1, times(1)).getFirstName();
        verify(person1, times(1)).getLastName();
    }

    /**
     * Test of create method when both names null, of class PersonService.
     */
    @Test
    public void testCreateBothNamesNull() {
        System.out.println("testCreateBothNamesNull");

        when(person1.getFirstName()).thenReturn(null);
        when(person1.getLastName()).thenReturn(null);

        assertThrows(IllegalArgumentException.class, () -> personService.create(person1));

        verify(person1, times(1)).getFirstName();
        verify(person1, times(0)).getLastName();
    }

    /**
     * Test of create method no role, of class PersonService.
     */
    @Test
    public void testCreateNoRole() {
        System.out.println("TestCreateNoRole");

        when(person1.getFirstName()).thenReturn(nameMock);
        when(person1.getLastName()).thenReturn(nameMock);
        when(person1.getRole()).thenReturn(null);
        when(personRepositoryMock.save(person1)).thenReturn(person1);
        when(person1.getRole()).thenReturn(Role.USER);

        assertEquals(Role.USER, personService.create(person1).getRole());

        verify(person1, times(1)).getFirstName();
        verify(person1, times(1)).getLastName();
        verify(person1, times(2)).getRole();
        verify(personRepositoryMock, times(1)).save(person1);
    }

    @Test
    void testUpdateSalary() {
        String targetTag = "testTag";
        Integer newSalary = 5000;
        LocalDateTime dateOfChange = LocalDateTime.now();

        when(person1.getSalary()).thenReturn(salaryManagerMock);
        when(salaryManagerMock.getCurrentSalary()).thenReturn(4000);
        when(personRepositoryMock.findByTag("testTag")).thenReturn(person1);
        when(salaryRepository.findLatestPaymentDateByPersonId(anyLong())).thenReturn(Optional.of(LocalDateTime.of(2024, 5, 24, 000000, 000000)));
        when(personRepositoryMock.updateSalary(targetTag, 4000, newSalary, dateOfChange)).thenReturn(1);

        int result = personService.updateSalary(targetTag, newSalary, dateOfChange);

        assertEquals(1, result);

    }

    @Test
    void testUpdateSalaryExceptionWhenDateOfChangeIsBeforeLastPayout() {
        String targetTag = "testTag";
        Integer newSalary = 5000;
        LocalDateTime dateOfChange = LocalDateTime.now();

        when(person1.getSalary()).thenReturn(salaryManagerMock);
        when(salaryManagerMock.getCurrentSalary()).thenReturn(4000);
        when(personRepositoryMock.findByTag("testTag")).thenReturn(person1);
        when(salaryRepository.findLatestPaymentDateByPersonId(anyLong())).thenReturn(Optional.of(LocalDateTime.of(2025, 5, 24, 000000, 000000)));
        when(personRepositoryMock.updateSalary(targetTag, 4000, newSalary, dateOfChange)).thenReturn(1);

        assertThrows(IllegalArgumentException.class, () -> personService.updateSalary(targetTag, newSalary, dateOfChange));

    }

    /**
     * Test of delete method first user, of class PersonService.
     */
    @Test
    public void testDelete() {
        System.out.println("delete");

        when(personRepositoryMock.findByTag(anyString())).thenReturn(person1);
        doNothing().when(personRepositoryMock).delete(person1);

        Person result = personService.delete(anyString());
        Person expResult = person1;
        assertEquals(expResult, result);
        
        when(personRepositoryMock.findByTag(anyString())).thenReturn(null);
        assertThrows(EntityNotFoundException.class, () -> personService.find("hej", "hejdå"));

        verify(personRepositoryMock, times(1)).delete(person1);
        verify(personRepositoryMock, times(2)).findByTag(anyString());
    }
}
