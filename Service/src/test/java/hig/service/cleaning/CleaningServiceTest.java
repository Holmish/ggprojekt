/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.service.cleaning;

import hig.domain.Cleaning;
import hig.domain.Person;
import hig.domain.Room;
import hig.repository.CleaningRepository;
import hig.repository.PersonRepository;
import hig.repository.RoomRepository;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author 21emho01
 */
public class CleaningServiceTest {

    private CleaningService cleaningService;
    @Mock
    private CleaningRepository cleaningRepositoryMock;
    @Mock
    private CleaningMapper cleaningMapperMock;
    @Mock
    private PersonRepository personRepositoryMock;
    @Mock
    private RoomRepository roomRepositoryMock;
    @Mock
    private Room roomMock;
    @Mock
    private Person personMock;
    @Mock
    private LocalDateTime to;
    @Mock
    private LocalDateTime from;
    @Mock
    private CleaningDTO cleaning1DTO;
    @Mock
    private CleaningDTO cleaning2DTO;
    @Mock
    private Cleaning cleaning1;
    @Mock
    private Cleaning cleaning2;

    public CleaningServiceTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        cleaningService = new CleaningService(cleaningRepositoryMock, personRepositoryMock, roomRepositoryMock, cleaningMapperMock);

        when(personMock.getTag()).thenReturn("7a3b09b9-e56c-49fb-a1d3-7fb9d61efabc");
        when(roomMock.getId()).thenReturn(1L);

    }

    @AfterEach
    public void tearDown() {
        cleaningService = null;
    }

    /**
     * Test of findByRoomId method, of class CleaningService.
     */
    @Test
    public void testFindByRoomId() {
        System.out.println("testFindByRoomId");
        List<Cleaning> cleaningList = List.of(cleaning1, cleaning2);
        List<CleaningDTO> cleaningListDTO = List.of(cleaning1DTO, cleaning2DTO);

        when(cleaningRepositoryMock.findByRoomIdAndTimeGreaterThan(any(Long.class), any(LocalDateTime.class))).thenReturn(cleaningList);
        when(cleaningMapperMock.toCleaningDtoList(cleaningList)).thenReturn(cleaningListDTO);

        List<CleaningDTO> expResult = cleaningListDTO;
        List<CleaningDTO> result = cleaningService.findByRoomId(1L);

        assertEquals(expResult, result);
        verify(cleaningRepositoryMock, times(1)).findByRoomIdAndTimeGreaterThan(any(Long.class), any(LocalDateTime.class));
        verify(cleaningMapperMock, times(1)).toCleaningDtoList(cleaningList);
    }
    
    /**
     * Test of findByRoomIdAndDays method, of class CleaningService.
     */
    @Test
    public void testFindByRoomIdAndDays() {
        System.out.println("testFindByRoomIdAndDays");
        
        when(cleaningMapperMock.toCleaningDtoList(anyList())).thenReturn(List.of(cleaning1DTO, cleaning2DTO));
        
        List<CleaningDTO> result = cleaningService.findByRoomIdAndDays(213L, 1L);
        List<CleaningDTO> expResult = List.of(cleaning1DTO, cleaning2DTO);
        
        assertEquals(expResult, result);
        verify(cleaningMapperMock, times(1)).toCleaningDtoList(anyList());

    }

    /**
     * Test of findByPersonIdAndInterval method, of class CleaningService.
     */
    @Test
    public void testFindByPersonIdAndIntervalFromAndToNull() {
        System.out.println("testFindByPersonIdAndIntervalFromAndToNull");

        List<Cleaning> listFromAndToNull = List.of(cleaning1, cleaning2);

        List<CleaningDTO> listFromAndToNullDTO = List.of(cleaning1DTO, cleaning2DTO);

        when(cleaningRepositoryMock.findByPersonId(1L)).thenReturn(listFromAndToNull);
        when(personMock.getId()).thenReturn(1L);
        when(cleaningMapperMock.toCleaningDtoList(listFromAndToNull)).thenReturn(listFromAndToNullDTO);
        when(personRepositoryMock.findByTag(anyString())).thenReturn(personMock);
        
        List<CleaningDTO> expResult = listFromAndToNullDTO;
        List<CleaningDTO> result = cleaningService.findByPersonIdAndInterval(anyString(), null, null);

        assertEquals(expResult, result);
        verify(cleaningRepositoryMock, times(1)).findByPersonId(1L);
        verify(cleaningMapperMock, times(1)).toCleaningDtoList(listFromAndToNull);
    }

    /**
     * Test of findByPersonIdAndInterval method, of class CleaningService.
     */
    @Test
    public void testFindByPersonIdAndIntervalFromNull() {
        System.out.println("testFindByPersonIdAndIntervalFromNull");

        List<Cleaning> listFromNull = List.of(cleaning1);
        List<CleaningDTO> listFromNullDTO = List.of(cleaning1DTO);

        when(cleaningRepositoryMock.findByPersonIdAndTimeLessThan(1L, to)).thenReturn(listFromNull);
        when(personMock.getId()).thenReturn(1L);
        when(cleaningMapperMock.toCleaningDtoList(listFromNull)).thenReturn(listFromNullDTO);
        when(personRepositoryMock.findByTag(anyString())).thenReturn(personMock);
        
        List<CleaningDTO> expResult = listFromNullDTO;
        List<CleaningDTO> result = cleaningService.findByPersonIdAndInterval(anyString(), null, to);

        assertEquals(expResult, result);
        verify(cleaningRepositoryMock, times(1)).findByPersonIdAndTimeLessThan(1L, to);
        verify(cleaningMapperMock, times(1)).toCleaningDtoList(listFromNull);
    }

    /**
     * Test of findByPersonIdAndInterval method, of class CleaningService.
     */
    @Test
    public void findByPersonIdAndIntervalToNull() {
        System.out.println("testFindByPersonIdAndIntervalToNull");

        List<Cleaning> listToNull = List.of(cleaning2);
        List<CleaningDTO> listToNullDTO = List.of(cleaning2DTO);

        when(personRepositoryMock.findByTag(anyString())).thenReturn(personMock);
        when(personMock.getId()).thenReturn(1L);
        when(cleaningRepositoryMock.findByPersonIdAndTimeGreaterThan(1L, from)).thenReturn(listToNull);
        when(cleaningMapperMock.toCleaningDtoList(listToNull)).thenReturn(listToNullDTO);
        
        List<CleaningDTO> expResult = listToNullDTO;
        List<CleaningDTO> result = cleaningService.findByPersonIdAndInterval(anyString(), from, null);

        assertEquals(expResult, result);
        verify(cleaningRepositoryMock, times(1)).findByPersonIdAndTimeGreaterThan(1L, from);
        verify(cleaningMapperMock, times(1)).toCleaningDtoList(listToNull);
    }

    /**
     * Test of findByPersonIdAndInterval method, of class CleaningService.
     */
    @Test
    public void findByPersonIdAndIntervalNonNull() {
        System.out.println("testFindByPersonIdAndIntervalNonNull");

        List<Cleaning> listNonNull = List.of(cleaning2, cleaning1);
        List<CleaningDTO> listNonNullDTO = List.of(cleaning2DTO, cleaning1DTO);

        when(cleaningRepositoryMock.findByPersonIdAndTimeGreaterThanAndTimeLessThan(1L, from, to)).thenReturn(listNonNull);
        when(personMock.getId()).thenReturn(1L);
        when(cleaningMapperMock.toCleaningDtoList(listNonNull)).thenReturn(listNonNullDTO);
        when(personRepositoryMock.findByTag(anyString())).thenReturn(personMock);

        List<CleaningDTO> expResult = listNonNullDTO;
        List<CleaningDTO> result = cleaningService.findByPersonIdAndInterval(anyString(), from, to);

        assertEquals(expResult, result);
        verify(cleaningRepositoryMock, times(1)).findByPersonIdAndTimeGreaterThanAndTimeLessThan(1L, from, to);
        verify(cleaningMapperMock, times(1)).toCleaningDtoList(listNonNull);
    }
    
    /**
     * Test of create method, of class CleaningService.
     */
    @Test
    public void testCreate() {
        System.out.println("testCreate");

        when(personRepositoryMock.findByTag(personMock.getTag())).thenReturn(personMock);
        when(personMock.getId()).thenReturn(1L);
        when(roomRepositoryMock.findById(roomMock.getId())).thenReturn(Optional.of(roomMock));
        when(cleaningRepositoryMock.save(any())).thenReturn(cleaning1);
        when(cleaningMapperMock.toCleaningDto((Cleaning) any())).thenReturn(cleaning1DTO);

        CleaningDTO expResult = cleaning1DTO;
        CleaningDTO result = cleaningService.create(1L, "7a3b09b9-e56c-49fb-a1d3-7fb9d61efabc");

        assertEquals(expResult, result);
        assertThrows(EntityNotFoundException.class, () -> cleaningService.create(1337L, "7a3b09b9-e56c-49fb-a1d3-7fb9d61efab123"));
        verify(personMock, times(1)).getTag();
        verify(personRepositoryMock, times(1)).findByTag(personMock.getTag());
        verify(roomMock, times(1)).getId();
        verify(roomRepositoryMock, times(1)).findById(roomMock.getId());
        verify(cleaningRepositoryMock, times(1)).save(any());
        verify(cleaningMapperMock, times(1)).toCleaningDto((Cleaning) any());

    }
}
