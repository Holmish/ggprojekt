/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.service.room;

import hig.domain.Cleaning;
import hig.domain.Name;
import hig.domain.Room;
import hig.repository.CleaningRepository;
import hig.repository.RoomRepository;
import hig.service.exceptions.IllegalActionException;
import jakarta.persistence.EntityNotFoundException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author 21masu04
 */
public class RoomServiceTest {

    private RoomService roomService;
    @Mock
    private RoomRepository roomRepositoryMock;
    @Mock
    private RoomMapper roomMapperMock;
    @Mock
    private CleaningRepository cleaningRepositoryMock;
    @Mock
    private Cleaning cleaningMock;
    @Mock
    private Room room1;
    @Mock
    private Room room2;
    @Mock
    private RoomDTO room1DTO;
    @Mock
    private RoomDTO room2DTO;
    @Mock
    private Name nameMock;

    public RoomServiceTest() {

    }

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        roomService = new RoomService(roomMapperMock, roomRepositoryMock, cleaningRepositoryMock);

        when(roomRepositoryMock.findById(1L)).thenReturn(Optional.of(room1));
        when(roomRepositoryMock.findById(2L)).thenReturn(Optional.empty());
        when(roomRepositoryMock.findById(3L)).thenReturn(Optional.of(room1));
    }

    @AfterEach
    public void tearDown() {
        roomService = null;
    }

    @Test
    public void testFind() {
        System.out.println("find");
        when(roomRepositoryMock.findById(1L)).thenReturn(Optional.of(room1));
        when(roomRepositoryMock.findById(2L)).thenReturn(Optional.empty());
        when(roomMapperMock.toRoomDto(room1)).thenReturn(room1DTO);

        RoomDTO expResult = room1DTO;
        RoomDTO result = roomService.find(1L);

        assertEquals(expResult, result);
        assertThrows(NoSuchElementException.class, () -> roomService.find(2L));
        verify(roomMapperMock, times(1)).toRoomDto(room1);
        verify(roomRepositoryMock, times(2)).findById(any());
    }

    /**
     * Test of findAll method, of class RoomService.
     */
    @Test
    public void testFindAll() {
        System.out.println("getAll");
        when(roomRepositoryMock.findAll()).thenReturn(List.of(room1, room2));
        when(roomMapperMock.toRoomDtoList(List.of(room1, room2))).thenReturn(List.of(room1DTO, room2DTO));

        List<RoomDTO> expResult = List.of(room1DTO, room2DTO);
        List<RoomDTO> result = roomService.findAll();

        assertEquals(expResult, result);
        verify(roomRepositoryMock, times(1)).findAll();
        verify(roomMapperMock, times(1)).toRoomDtoList(List.of(room1, room2));
    }

    /**
     * Test of create method, of class RoomService.
     */
    @Test
    public void testCreate() {
        System.out.println("create");
        when(room1.getName()).thenReturn(nameMock);
        when(roomRepositoryMock.save(room1)).thenReturn(room1);

        Room expResult = room1;
        Room result = roomService.create(room1);
        assertEquals(expResult, result);
        verify(room1, times(1)).getName();
        verify(roomRepositoryMock, times(1)).save(room1);
    }

    /**
     * Test of create method when null, of class RoomService.
     */
    @Test
    public void testCreateNull() {
        System.out.println("createNull");
        when(room1.getName()).thenReturn(null);
        assertThrows(IllegalArgumentException.class, () -> roomService.create(room1));
        verify(room1, times(1)).getName();
    }

    /**
     * Test of create method when null, of class RoomService.
     */
    @Test
    public void testDelete() {
        System.out.println("Delete");

        when(roomRepositoryMock.findById(1L)).thenReturn(Optional.of(room1));
        when(roomRepositoryMock.findById(2L)).thenReturn(Optional.of(room2));
        when(roomRepositoryMock.findById(3L)).thenReturn(Optional.empty());

        when(roomMapperMock.toRoomDto(room1)).thenReturn(room1DTO);
        when(roomMapperMock.toRoomDto(room2)).thenReturn(room2DTO);

        when(cleaningRepositoryMock.findByRoomId(1L)).thenReturn(List.of(cleaningMock));
        when(cleaningRepositoryMock.findByRoomId(2L)).thenReturn(List.of());

        assertThrows(IllegalActionException.class, () -> roomService.delete(1L));
        assertThrows(EntityNotFoundException.class, () -> roomService.delete(3L));

        RoomDTO expResult = room2DTO;
        RoomDTO result = roomService.delete(2L);

        assertEquals(expResult, result);
    }

}
