package hig.domain;

import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

/**
 *
 * @author 22sida01
 */
@Entity
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Embedded
    private Name name;

    private String comment;

    public Room() {

    }

    public Room(Name name, String comment) {
        this.name = name;
        setComment(comment);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public String getStringName() {
        return name.toString();
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        if (comment.length() > 500) {
            throw new IllegalArgumentException("Comment cannot be big");
        }
        this.comment = comment;
    }

}
