/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.domain;

import jakarta.persistence.Embeddable;
import java.time.LocalDateTime;

/**
 *
 * @author 22emfa02
 */
@Embeddable
public class SalaryManager {

    private LocalDateTime dateOfNewSalary;
    private Integer currentSalary;
    private Integer upcomingSalary;

    public SalaryManager(LocalDateTime changeOfDate, Integer currentSalary, Integer upcomingSalary) {
        setDateOfNewSalary(changeOfDate);
        setCurrentSalary(currentSalary);
        setUpcomingSalary(upcomingSalary);
    }

    public SalaryManager() {
    }

    public LocalDateTime getDateOfNewSalary() {
        return dateOfNewSalary;
    }

    public void setDateOfNewSalary(LocalDateTime dateOfNewSalary) {
        this.dateOfNewSalary = dateOfNewSalary;
    }

    public Integer getCurrentSalary() {
        return currentSalary;
    }

    public void setCurrentSalary(Integer currentSalary) {
        if(currentSalary<0) {
            throw new IllegalArgumentException("Salary cannot be negative");
        }
        this.currentSalary = currentSalary;
    }

    public Integer getUpcomingSalary() {
        return upcomingSalary;
    }

    public void setUpcomingSalary(Integer upcomingSalary) {
        if(upcomingSalary<0) {
            throw new IllegalArgumentException("Salary cannot be negative");
        }
        this.upcomingSalary = upcomingSalary;
    }

    public void updateSalary() {
        if (upcomingSalary != 0) {
            this.currentSalary = upcomingSalary;
            this.upcomingSalary = 0;
            this.dateOfNewSalary = null;
        }
    }
    @Override
    public String toString() {
        return String.format("Current salary: %d\nUpcoming salary: %d\nNew salary valid from: %s", currentSalary, upcomingSalary, dateOfNewSalary);
    }

}
