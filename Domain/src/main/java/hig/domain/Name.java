/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.domain;

import jakarta.persistence.Embeddable;

/**
 *
 * @author 21emho01
 */
@Embeddable
public class Name {

    private String name;

    public Name() {
    }

    public Name(String name) {
        setName(name);
    }

    public void setName(String name) {
        if (name.length() > 255) {
            throw new IllegalArgumentException("The given name is too long");
        }
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
