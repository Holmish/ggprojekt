package hig.domain;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.AttributeOverrides;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import java.time.Year;
import java.util.UUID;

/**
 *
 * @author thomas
 */
@Entity
public class Person {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String tag = UUID.randomUUID().toString();

    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "name", column = @Column(name = "first_name")),})
    private Name firstName;

    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name = "name", column = @Column(name = "last_name")),})
    private Name lastName;
    private Integer birthyear;
    private Role role;
    @Embedded
    private SalaryManager salary;    
    
    public Person(Long id, Name firstName, Name lastName, Integer birthyear, Role role, SalaryManager salary) {
        this(firstName, lastName, birthyear, role, salary);
        setId(id);
        
    }
    
    public Person(Name firstName, Name lastName, Integer birthyear, Role role, SalaryManager salary) {
        setFirstName(firstName);
        setLastName(lastName);
        setBirthyear(birthyear);
        setRole(role);
        setSalary(salary);
    }
    
    public Person() {
    }
    
    public Long getId() {
        return id;
    }
    
    public String getTag() {
        return tag;
    }
    
    public void setTag(String tag) {
        this.tag = tag;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public Name getFirstName() {
        return firstName;
    }
    
    public void setFirstName(Name firstName) {
        this.firstName = firstName;
    }
    
    public Name getLastName() {
        return lastName;
    }
    
    public void setLastName(Name lastName) {
        this.lastName = lastName;
    }
    
    public Integer getBirthyear() {
        return birthyear;
    }
    
    public void setBirthyear(Integer birthyear) {
        if (birthyear > Year.now().getValue()) {
            throw new IllegalArgumentException("Birthyear should not be in the future");
        }
        if (birthyear < 0) {
            throw new IllegalArgumentException("Birthyear should not be negative");
        }
        
        this.birthyear = birthyear;
    }
    
    public String getFullName() {
        return firstName + " " + lastName;
    }
    
    public Role getRole() {
        return role;
    }
    
    public void setRole(Role role) {
        this.role = role;
    }
    
    public SalaryManager getSalary() {
        return salary;
    }
    
    public void setSalary(SalaryManager salary) {
        this.salary = salary;
    }
    public String salaryInfo() {
        return salary.toString();
    }
    
}
