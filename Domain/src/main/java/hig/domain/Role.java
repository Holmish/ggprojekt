package hig.domain;

/**
 *
 * @author 21emho01
 */
public enum Role {
    ADMINISTRATOR,
    USER;
}
