/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.domain;

import java.time.LocalDateTime;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author 21emho01
 */
public class PersonTest {

    private Person testPerson;

    @Mock
    private Name firstNameMock;
    @Mock
    private Name lastNameMock;

    public PersonTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {

        MockitoAnnotations.initMocks(this);
        when(firstNameMock.toString()).thenReturn("Anders");
        when(lastNameMock.toString()).thenReturn("Åkesson");
        testPerson = new Person(firstNameMock, lastNameMock, 2012, Role.USER, new SalaryManager(LocalDateTime.of(2025,04,14,00,00,00,000000), 400, 500));
    }

    @AfterEach
    public void tearDown() {
        testPerson = null;
        firstNameMock = null;
        lastNameMock = null;
    }

    /**
     * Test of getId method, of class Person.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        testPerson.setId(1L);
        Long expResult = 1L;
        Long result = testPerson.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTag method, of class Person.
     */
    @Test
    public void testGetTag() {
        System.out.println("getTag");
        String tag = testPerson.getTag();
        System.out.println(tag);
        String expResult = tag;
        String result = testPerson.getTag();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTag method, of class Person.
     */
    @Test
    public void testSetTag() {
        System.out.println("setTag");
        String expResult = "43829473892478329";
        testPerson.setTag(expResult);
        String result = testPerson.getTag();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Person.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        Long expResult = 2L;
        testPerson.setId(expResult);
        Long result = testPerson.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFirstName method, of class Person.
     */
    @Test
    public void testGetFirstName() {
        System.out.println("getFirstName");

        String expResult = "Anders";
        assertEquals(expResult, testPerson.getFirstName().toString());
    }

    /**
     * Test of setFirstName method, of class Person.
     */
    @Test
    public void testSetFirstName() {
        System.out.println("setFirstName");

        testPerson.setFirstName(new Name("Sixten"));
        assertEquals("Sixten", testPerson.getFirstName().toString());

    }

//    /**
//     * Test of getLastName method, of class Person.
//     */
    @Test
    public void testGetLastName() {
        System.out.println("getLastName");

        String expResult = "Åkesson";
        assertEquals(expResult, testPerson.getLastName().toString());
    }

//    /**
//     * Test of setLastName method, of class Person.
//     */
    @Test
    public void testSetLastName() {
        System.out.println("setLastName");

        testPerson.setLastName(new Name("Kingsson"));
        assertEquals("Kingsson", testPerson.getLastName().toString());
    }

    /**
     * Test of getBirthyear method, of class Person.
     */
    @Test
    public void testGetBirthyear() {
        System.out.println("getBirthyear");
        testPerson.setBirthyear(1999);
        Integer expResult = 1999;
        Integer result = testPerson.getBirthyear();
        assertEquals(expResult, result);
    }

    /**
     * Test of setBirthyear method, of class Person.
     */
    @Test
    public void testSetBirthyear() {
        System.out.println("setBirthyear");
        testPerson.setBirthyear(2002);
        Integer expResult = 2002;
        Integer result = testPerson.getBirthyear();
        assertEquals(expResult, result);
        assertThrows(IllegalArgumentException.class, () -> testPerson.setBirthyear(-1),
                "Testing that setBirthyear throws IllegalArgumentException when birthyear is negative");
        assertThrows(IllegalArgumentException.class, () -> testPerson.setBirthyear(2025),
                "Testing that setBirthyear throws IllegalArgumentException when birthyear bigger than current year");

        testPerson.setBirthyear(0);
        expResult = 0;
        assertEquals(expResult, testPerson.getBirthyear());

        testPerson.setBirthyear(2024);
        expResult = 2024;
        assertEquals(expResult, testPerson.getBirthyear());
    }

//    /**
//     * Test of getFullName method, of class Person.
//     */
    @Test
    public void testGetFullName() {
        System.out.println("getFullName");

        String expResult = "Anders Åkesson";
        assertEquals(expResult, testPerson.getFullName());
    }

    /**
     * Test of getRole method, of class Person.
     */
    @Test
    public void testGetRole() {
        System.out.println("getRole");
        Role expResult = Role.USER;
        testPerson.setRole(Role.USER);
        Role result = testPerson.getRole();
        assertEquals(expResult, result);

        testPerson.setRole(Role.ADMINISTRATOR);
        expResult = Role.ADMINISTRATOR;
        result = testPerson.getRole();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetRole() {
        System.out.println("setRole");
        Role expResult = Role.USER;
        testPerson.setRole(Role.USER);
        assertEquals(testPerson.getRole(), expResult);

        expResult = Role.ADMINISTRATOR;
        testPerson.setRole(Role.ADMINISTRATOR);
        assertEquals(testPerson.getRole(), expResult);

    }

}
