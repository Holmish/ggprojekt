/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.domain;

import java.time.LocalDateTime;
import java.time.Month;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.mockito.Mock;

/**
 *
 * @author 21masu04
 */
public class SalaryManagerTest {

    private SalaryManager instance;

    @Mock
    LocalDateTime localDateTime;

    public SalaryManagerTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        instance = new SalaryManager(localDateTime.of(24, Month.MARCH, 12, 12, 5),300,325);

    }

    @AfterEach
    public void tearDown() {
        instance = null;
    }

    /**
     * Test of getDateOfNewSalary method, of class SalaryManager.
     */
    @Test
    public void testGetDateOfNewSalary() {
        System.out.println("getDateOfNewSalary");
        LocalDateTime expResult = localDateTime.of(24, Month.MARCH, 12, 12, 5);
        LocalDateTime result = instance.getDateOfNewSalary();
        assertEquals(expResult, result);

    }

    /**
     * Test of setDateOfNewSalary method, of class SalaryManager.
     */
    @Test
    public void testSetDateOfNewSalary() {
        System.out.println("setDateOfNewSalary");
        
        instance.setDateOfNewSalary(localDateTime.of(24, Month.APRIL, 12, 12, 5));
        LocalDateTime expResult = localDateTime.of(24, Month.APRIL, 12, 12, 5);
        LocalDateTime result = instance.getDateOfNewSalary();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCurrentSalary method, of class SalaryManager.
     */
    @Test
    public void testGetCurrentSalary() {
        System.out.println("getCurrentSalary");
        
       Integer result = instance.getCurrentSalary();
       Integer expResult = 300;
       assertEquals(expResult, result);
    }

    /**
     * Test of setCurrentSalary method, of class SalaryManager.
     */
    @Test
    public void testSetCurrentSalary() {
        System.out.println("setCurrentSalary");
        instance.setCurrentSalary(500);
        Integer result = instance.getCurrentSalary();
        Integer expResult = 500;
        assertEquals(expResult,result);
        assertThrows(IllegalArgumentException.class,() -> instance.setCurrentSalary(-500));
    }

    /**
     * Test of getUpcomingSalary method, of class SalaryManager.
     */
    @Test
    public void testGetUpcomingSalary() {
        System.out.println("getUpcomingSalary");
        Integer result = instance.getUpcomingSalary();
        Integer expResult = 325;
        assertEquals(expResult,result);
    }

    /**
     * Test of setUpcomingSalary method, of class SalaryManager.
     */
    @Test
    public void testSetUpcomingSalary() {
        instance.setUpcomingSalary(500);
        Integer result = instance.getUpcomingSalary();
        Integer expResult = 500;
        assertEquals(expResult,result);
        assertThrows(IllegalArgumentException.class,() -> instance.setUpcomingSalary(-500));
    }

    /**
     * Test of updateSalary method, of class SalaryManager.
     */
    @Test
    public void testUpdateSalary() {
        System.out.println("updateSalary");
        instance.updateSalary();
        Integer expResult = 325;
        Integer result = instance.getCurrentSalary();
        assertEquals(expResult,result);
    }

    /**
     * Test of toString method, of class SalaryManager.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = String.format("Current salary: %d\nUpcoming salary: %d\nNew salary valid from: %s",
                instance.getCurrentSalary(),instance.getUpcomingSalary() , instance.getDateOfNewSalary());
        String result = instance.toString();
        assertEquals(expResult,result);
        
    }

}
