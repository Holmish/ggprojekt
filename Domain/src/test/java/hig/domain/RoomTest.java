package hig.domain;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Clara Ersson 
 * @author Emelie Färnstrand 
 */
public class RoomTest {
    
    private Room testRoom;
    
    public RoomTest() {
    }
    
    @BeforeEach
    public void setUp() {
        testRoom = new Room(new Name("Casa de Clara"), "Väldigt dålig ytterdörr.");
        testRoom.setId(1L);
    }
    
    @AfterEach
    public void tearDown() {
        testRoom = null;
    }

    /**
     * Test of getId method, of class Room.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        Long expResult = 1L;
        Long result = testRoom.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Room.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        Long id = 3L;
        testRoom.setId(id);
        Long expResult = 3L;
        Long result = testRoom.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getName method, of class Room.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Name expResult = new Name("Casa de Clara");
        Name result = testRoom.getName();
        assertEquals(expResult.toString(), result.toString());
    }

    /**
     * Test of setName method, of class Room.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        Name name = new Name("Emelies lya");
        testRoom.setName(name);
        Name expResult = name;
        Name result = testRoom.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getStringName method, of class Room.
     */
    @Test
    public void testGetStringName() {
        System.out.println("getStringName");
        String expResult = "Casa de Clara";
        String result = testRoom.getStringName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getComment method, of class Room.
     */
    @Test
    public void testGetComment() {
        System.out.println("getComment");
        String expResult = "Väldigt dålig ytterdörr.";
        String result = testRoom.getComment();
        assertEquals(expResult, result);
    }

    /**
     * Test of setComment method, of class Room.
     */
    @Test
    public void testSetComment() {
        System.out.println("setComment");
        String expResult = "Ytterdörren är lagad!";
        testRoom.setComment(expResult);
        String result = testRoom.getComment();
        assertEquals(expResult, result);
    }
    
}
