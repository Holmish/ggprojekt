/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.domain;

import java.time.LocalDateTime;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author 21masu04
 */
public class SalaryTest {

    @Mock
    Person testPerson;
    @Mock
    Person testPerson2;

    @Mock
    LocalDateTime localDateTime;

    Salary instance;

    public SalaryTest() {

        MockitoAnnotations.initMocks(this);
        instance = new Salary(1l, testPerson, localDateTime, 300);

    }

    /**
     * Test of getId method, of class Salary.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");

        Long expResult = 1l;
        Long result = instance.getId();

        assertEquals(expResult, result);

    }

    /**
     * Test of setId method, of class Salary.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");

        Long expResult = 2l;
        instance.setId(2l);
        Long result = instance.getId();

        assertEquals(expResult, result);

    }

    /**
     * Test of getPerson method, of class Salary.
     */
    @Test
    public void testGetPerson() {
        System.out.println("getPerson");

        Person expResult = testPerson;
        Person result = instance.getPerson();

        assertEquals(expResult, result);

    }

    /**
     * Test of setPerson method, of class Salary.
     */
    @Test
    public void testSetPerson() {
        System.out.println("setPerson");

        Person expResult = testPerson2;
        instance.setPerson(testPerson2);
        Person result = instance.getPerson();

        assertEquals(expResult, result);

    }

    /**
     * Test of getDate method, of class Salary.
     */
    @Test
    public void testGetDate() {
        System.out.println("getDate");
        
        LocalDateTime expResult = localDateTime;
        LocalDateTime result = instance.getDate();
        
         assertEquals(expResult, result);

    }

    /**
     * Test of setDate method, of class Salary.
     */
    @Test
    public void testSetDate() {
        System.out.println("setDate");
        
       
        
        LocalDateTime expResult = LocalDateTime.of(24, 3, 2, 0, 0);
        instance.setDate(LocalDateTime.of(24, 3, 2, 0, 0));
        LocalDateTime result = instance.getDate();
        
         assertEquals(expResult, result);

    }

    /**
     * Test of getSalary method, of class Salary.
     */
    @Test
    public void testGetSalary() {
        System.out.println("getSalary");
        
        Integer expResult = 300;
        Integer result = instance.getSalary();
        
        assertEquals(expResult, result);

    }

    /**
     * Test of setSalary method, of class Salary.
     */
    @Test
    public void testSetSalary() {
        System.out.println("setSalary");
        
        Integer expResult = 400;
        instance.setSalary(400);
        Integer result = instance.getSalary();
        
        assertEquals(expResult, result);
        
        
        assertThrows(IllegalArgumentException.class, ()-> instance.setSalary(-1337));

    }

}
