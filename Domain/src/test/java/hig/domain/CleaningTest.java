/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.domain;

import java.time.LocalDateTime;
import java.time.Month;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author 21emho01
 */
public class CleaningTest {

    private Cleaning testCleaning;

    @Mock
    private Room roomMock;

    @Mock
    private Person personMock;

    public CleaningTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        testCleaning = new Cleaning();
        MockitoAnnotations.initMocks(this);

    }

    @AfterEach
    public void tearDown() {
        testCleaning = null;
    }

    /**
     * Test of getId method, of class Cleaning.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        testCleaning.setId(7L);
        Long expResult = 7L;
        Long result = testCleaning.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Cleaning.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        testCleaning.setId(7935L);
        Long expResult = 7935L;
        Long result = testCleaning.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTime method, of class Cleaning.
     */
    @Test
    public void testGetTime() {
        System.out.println("getTime");
        LocalDateTime expResult = LocalDateTime.now().withNano(0).withSecond(0);
        testCleaning = new Cleaning();
        LocalDateTime result = testCleaning.getTime();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTime method, of class Cleaning.
     */
    @Test
    public void testSetTime() {
        System.out.println("setTime");
        LocalDateTime expResult = LocalDateTime.of(1802, Month.MARCH, 25, 10, 55, 23);
        testCleaning.setTime(expResult);
        LocalDateTime result = testCleaning.getTime();
        assertEquals(expResult, result);
    }

    /**
     * Test of getRoom method, of class Cleaning.
     */
    @Test
    public void testGetRoom() {
        System.out.println("getRoom");
        testCleaning = new Cleaning(roomMock, personMock);
        Room expResult = roomMock;
        Room result = testCleaning.getRoom();
        assertEquals(expResult, result);
    }

    /**
     * Test of setRoom method, of class Cleaning.
     */
    @Test
    public void testSetRoom() {
        System.out.println("setRoom");
        testCleaning.setRoom(roomMock);
        Room expResult = roomMock;
        Room result = testCleaning.getRoom();
        assertEquals(expResult, result);

    }

    /**
     * Test of getPerson method, of class Cleaning.
     */
    @Test
    public void testGetPerson() {
        System.out.println("getPerson");
        testCleaning = new Cleaning(roomMock, personMock);
        Person expResult = personMock;
        Person result = testCleaning.getPerson();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPerson method, of class Cleaning.
     */
    @Test
    public void testSetPerson() {
        System.out.println("setPerson");
        testCleaning.setPerson(personMock);
        Person expResult = personMock;
        Person result = testCleaning.getPerson();
        assertEquals(expResult, result);
    }

}
