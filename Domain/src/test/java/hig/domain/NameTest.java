/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package hig.domain;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author 21emho01
 */
public class NameTest {

    private Name name;

    public NameTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        name = new Name("MaxProtector");
    }

    @AfterEach
    public void tearDown() {
        name = null;
    }

    @Test
    public void setName() {
        name.setName("Torsten");
        String expResult = "Torsten";
        assertEquals(expResult, name.getName());

        assertThrows(IllegalArgumentException.class,
                () -> name.setName("UiEOAHzFguHhENOrgeUPNQCsRTIxDeitnaMVqMsbUWOCedSoYrxnCbIOyLCbGLdShtyVfJzjwfeeINZasAhpksrSqGsVocDZKvuBiNlGLwfOEvqesXmEazGccuCbAOkqnPCECjdqQGUjtXUBNfDKWTcoByxoTqhTROCKuvVpEUzyAzoAznjMJPEKlGwejFLMBlxGrowErhDjBWolqilJwGXfcOhDFvfCutGrZTVpKZqPGKpIXXHDHRpzAsXJSQRb"));

        assertDoesNotThrow(() -> name.setName("UiEOAHzFguHhENOrgeUPNQCsRTIxDeitnaMVqMsbUWOCedSoYrxnCbIOyLCbGLdShtyVfJzjwfeeINZasAhpksrSqGsVocDZKvuBiNlGLwfOEvqesXmEazGccuCbAOkqnPCECjdqQGUjtXUBNfDKWTcoByxoTqhTROCKuvVpEUzyAzoAznjMJPEKlGwejFLMBlxGrowErhDjBWolqilJwGXfcOhDFvfCutGrZTVpKZqPGKpIXXHDHRpzAsXJSQb"));
    }

    @Test
    public void getName() {
        String expResult = "MaxProtector";
        assertEquals(expResult, name.getName());
    }

    @Test
    public void testNameToString() {
        String result = this.name.toString();
        String expResult = "MaxProtector";
        assertEquals(expResult, result);
    }
}
