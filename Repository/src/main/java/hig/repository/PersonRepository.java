package hig.repository;

import hig.domain.Person;
import hig.domain.Role;
import jakarta.transaction.Transactional;
import java.time.LocalDateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomas
 */
@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    public Person findByTag(String targetTag);

    @Modifying
    @Transactional
    @Query("UPDATE Person p SET p.salary.currentSalary = :currentSalary, p.salary.upcomingSalary = :upcomingSalary, p.salary.dateOfNewSalary = :dateOfNewSalary WHERE p.tag = :tag")
    public int updateSalary(@Param("tag") String tag, @Param("currentSalary") Integer currentSalary, @Param("upcomingSalary") Integer upcomingSalary, @Param("dateOfNewSalary") LocalDateTime changeOfDate);
    
    
}
