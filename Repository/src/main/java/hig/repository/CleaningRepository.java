package hig.repository;

import hig.domain.Cleaning;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author 22emfa02
 */
@Repository
public interface CleaningRepository extends JpaRepository<Cleaning, Long> {

    public List<Cleaning> findByRoomIdAndTimeGreaterThan(Long roomId, LocalDateTime from);
    
    public List<Cleaning> findByRoomId(Long roomId);
    
    public List<Cleaning> findByPersonIdAndTimeGreaterThanAndTimeLessThan(Long personId, LocalDateTime from, LocalDateTime to);
    
    public List<Cleaning> findByPersonIdAndTimeGreaterThan(Long personId, LocalDateTime from);
    
    public List<Cleaning> findByPersonIdAndTimeLessThan(Long personId, LocalDateTime to);
    
    public List<Cleaning> findByPersonId(Long personId);
}
