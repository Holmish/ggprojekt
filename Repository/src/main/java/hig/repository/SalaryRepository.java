/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hig.repository;

import hig.domain.Salary;
import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author 21emho01
 */
@Repository
public interface SalaryRepository extends JpaRepository<Salary, Long> {

    @Query("SELECT MAX(s.date) FROM Salary s WHERE s.person.id = ?1")
    Optional<LocalDateTime> findLatestPaymentDateByPersonId(Long personId);

}
